# LBSim #

LBSim stands for **L**oad **B**alancing **Sim**ulator.

LBSim is a specialized toolkit for developing and simulation various load balancing algorithm. It is based on MSG API of SimGrid ([http://simgrid.gforge.inria.fr/]("SimGrid")).

## Getting started ##

### Requirements ###
* Gradle ([http://www.gradle.org]("Gradle"))

### Additional requirements on Windows ###

1. Download and install [http://mingw-w64.org/]("MinGW-W64")
1. ![](install_wingw.png)
1. After installation add "<path-to-mingw64-dist>\mingw64\bin" to the PATH environment
1. (reboot)

### Build & Run ###
1. Fetch the latest version from repository
1. Build all with the command `gradle build`
1. Navigate to directory `./build/distribution` and decompress either `LBSim-xx.zip` or `LBSim-xx.tar`
1. Copy `platform.xml`, `deployment.xml` and `scenario01.txt` to `./build/distribution/LBSim-xx/bin/` directory
1. Run `LBSim` or `LBSim.bat`