package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.task.TasksInfo;
import org.simgrid.msg.Host;

/**
 * State-Info-Provider Mock for tests
 */
public class StateInfoProviderMock extends StateInfoProvider {
    public StateInfoProviderMock(Host host, String name, String[] args) {
        super(host, name, args);
    }

    @Override
    protected void setupArguments(String[] args) {

    }

    @Override
    public void update(TasksInfo ti, SourceEvent event) {

    }

    @Override
    protected double periodicInterval() {
        return 0;
    }

    @Override
    protected void periodicActivator() {

    }

    @Override
    protected void shutdown() {

    }
}
