package at.enfilo.lbsim.model.statistics;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.simgrid.msg.HostNotFoundException;

import java.io.IOException;
import java.util.Random;

public class SubtasksScenarioTest extends ScenarioTest {
    private Random rnd;
    private int tasks;

    @Before
    public void setUp() {
        rnd = new Random();
        tasks = 0;
    }

    @Test
    public void regularScenario() throws HostNotFoundException, IOException {
        runSimulation();
    }

    @Override
    protected String generateTaskString() {
        //# type (j=job, t=task, s=sub-task) ; time to wait ; computation duration (flops) ; task-size (bytes) ; additional required data (bytes) ; result size (bytes) [; # recursion, default = 0]
        double computationDuration = rnd.nextDouble() * 100;
        double taskSize = rnd.nextDouble() * 100;
        double requiredData = rnd.nextDouble() * 100;
        double resultData = rnd.nextDouble() * 100;
        int subs = rnd.nextInt(3);
        tasks += subs + 1;
        String rv = "t;0.0;" + computationDuration + ";" + taskSize + ";" + requiredData + ";" + resultData + "\n";
        for (int i = 0; i < subs; i++) {
            computationDuration = rnd.nextDouble() * 100;
            taskSize = rnd.nextDouble() * 100;
            requiredData = rnd.nextDouble() * 100;
            resultData = rnd.nextDouble() * 100;
            rv += "s;0.0;" + computationDuration + ";" + taskSize + ";" + requiredData + ";" + resultData + "\n";
        }
        return rv;
    }

    @Override
    protected int getTaskCount() {
        return tasks;
    }

    @After
    public void tearDown() throws Exception {
        tasks = 0;
        rnd = new Random();
    }
}
