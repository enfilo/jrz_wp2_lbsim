package at.enfilo.lbsim.model.statistics;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.simgrid.msg.HostNotFoundException;

import java.io.IOException;
import java.util.Random;

public class RecursionScenarioTest extends ScenarioTest {
    private int tasks;
    private Random rnd;

    @Before
    public void setUp() throws Exception {
        tasks = 0;
        rnd = new Random();
    }

    @Test
    public void recursionScenario() throws HostNotFoundException, IOException {
        runSimulation();
    }

    @Override
    protected String generateTaskString() {
        //# type (j=job, t=task, s=sub-task) ; time to wait ; computation duration (flops) ; task-size (bytes) ; additional required data (bytes) ; result size (bytes) [; # recursion, default = 0]
        double computationDuration = rnd.nextDouble() * 100;
        double taskSize = rnd.nextDouble() * 100;
        double requiredData = rnd.nextDouble() * 100;
        double resultData = rnd.nextDouble() * 100;
        int recursions = rnd.nextInt(3);
        tasks += recursions + 1;
        return "t;0.0;" + computationDuration + ";" + taskSize + ";" + requiredData + ";" + resultData + ";" + recursions;
    }

    @Override
    protected int getTaskCount() {
        return tasks;
    }

    @After
    public void tearDown() throws Exception {
        tasks = 0;
        rnd = new Random();
    }
}
