package at.enfilo.lbsim.model.statistics;

import org.junit.After;
import org.junit.Test;
import org.simgrid.msg.HostNotFoundException;
import org.simgrid.msg.Msg;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Setup a minimal environment, consists of:
 * 1x client
 * 1x broker
 * 1x roundrobin scheduler
 * 1x dataprovider
 * 1x worker (incl. state-info-provider)
 *
 * Create job and tasks and submit it.
 */
public abstract class ScenarioTest {
    private String scenarioFile;
    private int jobs;
    private int tasks;

    private void createScenarioFile() throws IOException {
        scenarioFile = "integrationTestScenario.txt";
        Random rnd = new Random();
        jobs = rnd.nextInt(4) + 1;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(scenarioFile))) {
            for (int j = 0; j < jobs; j++) {
                writer.write("j;0.0");
                writer.newLine();
                for (int t = 0; t < (rnd.nextInt(4) + 1); t++) {
                    String task = generateTaskString();
                    writer.write(task);
                    writer.newLine();
                }
            }
        }
    }

    /**
     * # type (j=job, t=task, s=sub-task) ; time to wait ; computation duration (flops) ; task-size (bytes) ; additional required data (bytes) ; result size (bytes) [; # recursion, default = 0]
     */
    protected abstract String generateTaskString();

    protected abstract int getTaskCount();

    protected void runSimulation() throws IOException {
        Msg.init(new String[]{});
        createScenarioFile();
        tasks = getTaskCount();
        Msg.createEnvironment("platform_integrationTest.xml");
        Msg.deployApplication("deployment_integrationTest.xml");
        Msg.run();
        checkSimulationStatistics();
    }


    private void checkSimulationStatistics() {
        assertTrue(Msg.getClock() > 0);
        assertEquals(jobs, JobStatistics.getJobs().size());
        JobStatistics.getJobs().forEach(jobData -> assertTrue(jobData.isFinished()));
        int countTasks = 0;
        assertEquals(tasks, JobStatistics.getJobs().stream().mapToInt(jobData -> jobData.getNumberOfTasks()).sum());
    }

    @After
    public void shutdown() {
        File f = new File(scenarioFile);
        f.delete();
        JobStatistics.reset();
        jobs = 0;
        tasks = 0;
    }
}
