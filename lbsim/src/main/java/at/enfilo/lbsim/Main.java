package at.enfilo.lbsim;

import at.enfilo.lbsim.model.statistics.JobStatistics;
import at.enfilo.lbsim.model.statistics.WorkerStatistics;
import at.enfilo.lbsim.model.util.RandomUtil;
import at.enfilo.lbsim.statisticsviewer.JobStatisticViewer;
import at.enfilo.lbsim.statisticsviewer.WorkerStatisticsViewer;
import org.simgrid.msg.Msg;
import org.simgrid.msg.NativeException;

import java.io.File;

/**
 *
 * @author soma
 */
public class Main {
	
	public static void main(String[] args) throws NativeException {
		if (args.length == 0) {
			System.out.println("Arguments: <platform-file.xml> <deployment-file.xml>");
			System.out.println("  Default: platform.xml deployment.xml");
		}

		// initialize the MSG simulation. Must be done before anything else (even logging)
		//String[] tracingArgs = new String[]{"--cfg=tracing:yes", "--cfg=tracing/uncategorized:yes", "--cfg=tracing/msg/process:yes"};
		//String[] tracingArgs = new String[]{"--log=root.app:file:simgrid.log"};
		String[] tracingArgs = new String[]{"--log=root.:info"};
		//String[] tracingArgs = new String[]{"--cfg=msg/debug-multiple-use:on"};
		//String[] tracingArgs = new String[]{"--cfg=tracing:yes", "--cfg=tracing/msg/process:yes"};

		Msg.init(tracingArgs);

		RandomUtil.init(123456789);

		String platform = args.length > 1 ? args[0] : "platform.xml";
		String deploy = args.length > 1 ? args[1] : "deployment.xml";
		//String deploy = args.length > 1 ? args[1] : "deployment_def.xml";
		//String deploy = args.length > 1 ? args[1] : "deployment_global.xml";
		//String deploy = args.length > 1 ? args[1] : "deployment_an.xml";
		//String deploy = args.length > 1 ? args[1] : "deployment_rv.xml";

		//String platform = "./scenarios/01/platform_2clusters.xml";
		//String deploy = "./scenarios/01/deployment_rv_2clusters.xml";
		
		Msg.info("Platform: " + platform);
		Msg.info("Deployment: " + deploy);
		Msg.info("Current directory: " + new File(".").getAbsolutePath());

		// construct the platform and deploy the application
		Msg.createEnvironment(platform);
		Msg.deployApplication(deploy);

		//  execute the simulation.
		Msg.run();

		// Show statistics
		WorkerStatisticsViewer.main(new String[]{WorkerStatistics.DEFAULT_WORKER_FILE_NAME, WorkerStatistics.DEFAULT_QUEUE_FILE_NAME});
		JobStatisticViewer.main(new String[]{JobStatistics.DEFAULT_JOB_FILE_NAME, JobStatistics.DEFAULT_TASK_FILE_NAME});

	}
}
