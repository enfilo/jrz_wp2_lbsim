package at.enfilo.lbsim.model.task;

public enum TaskType {
	STATE_INFO,
	TASK,
	TASKS_INFO,
	ORDER,
	DATA,
	CUSTOM
}
