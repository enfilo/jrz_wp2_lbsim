package at.enfilo.lbsim.model.task;

public class Data extends AbstractTask {
	private static final String NAME_PREFIX = "Data_";
	private static long idCounter = 0;

	public Data(double bytesAmount) {
		super(TaskType.DATA, NAME_PREFIX + (idCounter++), 0, bytesAmount);
	}
}
