package at.enfilo.lbsim.model.statistics;

import org.simgrid.msg.Msg;

class TaskData {
    private final int taskId;
	private final int order;
    private final double creationTime;
    private double startFetchDataTime;
    private double endFetchDataTime;
	private double startExecTime;
	private double endExecTime;
	private double startStoreResultTime;
	private double endStoreResultTime;
	private double finishedTime;
	private int moveCounter;
	private double startQueueTime;
	private double endQueueTime;

	TaskData(int taskId, int parentTaskId) {
        this.taskId = taskId;
		this.order = parentTaskId;
        creationTime = Msg.getClock();
    }

	int getTaskId() {
		return taskId;
	}

	double getCreationTime() {
		return creationTime;
	}

	double getStartExecTime() {
		return startExecTime;
	}

	double getEndExecTime() {
		return endExecTime;
	}

	void startExec() {
		startExecTime = Msg.getClock();
	}

	void endExec() {
		endExecTime = Msg.getClock();
	}

	boolean isFinished() {
		return finishedTime > 0;
	}

	void moveTask() {
		moveCounter++;
	}

	int getMoveCounter() {
		return moveCounter;
	}

	void finished() {
		finishedTime = Msg.getClock();
	}

	void startFetchData() {
		startFetchDataTime = Msg.getClock();
	}

	double getStartFetchDataTime() {
		return startFetchDataTime;
	}

	void endFetchData() {
		endFetchDataTime = Msg.getClock();
	}

	void startStoreResult() {
		startStoreResultTime = Msg.getClock();
	}

	void endStoreResult() {
		endStoreResultTime = Msg.getClock();
	}

	double getStartStoreResultTime() {
		return startStoreResultTime;
	}

	double getEndStoreResultTime() {
		return endStoreResultTime;
	}

	double getFinishedTime() {
		return finishedTime;
	}

	void startQueue() {
		if (startQueueTime == 0) {
			startQueueTime = Msg.getClock();
		}
	}

	double getEndFetchDataTime() {
		return endFetchDataTime;
	}

	double getStartQueueTime() {
		return startQueueTime;
	}

	void endQueue() {
		endQueueTime = Msg.getClock();
	}

	double getEndQueueTime() {
		return endQueueTime;
	}

	int getOrder() {
		return order;
	}
}
