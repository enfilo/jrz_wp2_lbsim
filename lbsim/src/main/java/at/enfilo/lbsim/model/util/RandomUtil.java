package at.enfilo.lbsim.model.util;

import java.util.Random;

public class RandomUtil {
	private static Random rnd;

	public static void init(long seed) {
		rnd = new Random(seed);
	}

	public static Random getInstance() {
		if (rnd == null) {
			rnd = new Random();
		}
		return rnd;
	}
}
