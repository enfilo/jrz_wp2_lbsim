package at.enfilo.lbsim.model.util;

import at.enfilo.lbsim.model.process.BaseProcess;
import org.simgrid.msg.Host;
import org.simgrid.msg.MsgException;
import org.simgrid.msg.Process;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


/**
 * Helps to startup processes on a many nodes (cluster).
 */
public class ProcessLauncher extends Process {
	/**
	 * Constructs a new process from a host and his name, the arguments of here method function are
	 * specified by the parameter args.
	 *
	 * @param host The host of the process to create.
	 * @param name The name of the process.
	 * @param args The arguments of main method of the process.
	 */
	public ProcessLauncher(Host host, String name, String[] args) {
		super(host, name, args);
	}


	/**
	 * The main function of the process (to implement).
	 *
	 * @param args
	 * @throws MsgException
	 */
	@Override
	public void main(String[] args) throws MsgException {
		if (args.length < 2) {
			throw new RuntimeException("ProcessLauncher needs at least 2 arguments: name of nodes (radical) and process name");
		}

		// Parse main args: host names and process/function
		String nodes = args[0];
		String baseName = nodes.replaceAll("^(\\D+).*", "$1");
		String[] range = nodes.replaceAll(".*(\\d+-\\d+)$", "$1").split("-");
		if (range.length != 2) {
			throw new RuntimeException("name of node must be a radical like cluster configuration. Example: 'worker0-99'.");
		}
		String processName = args[1];

		try {
			// Check if process is from type BaseProcess
			Class<?> cls = Class.forName(processName);
			if (BaseProcess.class.isAssignableFrom(cls)) {
				Class<BaseProcess> baseProcessClass = (Class<BaseProcess>) cls;
				Constructor<BaseProcess> constructor = baseProcessClass.getConstructor(Host.class, String.class, String[].class);
				// Adapt args for process
				String[] processArgs = new String[args.length - 2];
				System.arraycopy(args, 2, processArgs, 0, args.length - 2);
				// Loop trough hosts and start the process
				int i = Integer.parseInt(range[0]);
				int end = Integer.parseInt(range[1]);
				for (; i <= end; i++) {
					Host h = Host.getByName(baseName + i);
					BaseProcess bp = constructor.newInstance(h, processName, processArgs);
					bp.start();
				}
			}

		} catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
