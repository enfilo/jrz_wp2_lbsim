package at.enfilo.lbsim.model.task;

import org.simgrid.msg.Task;


public abstract class AbstractTask extends Task {
	public static final int BYTE_SIZE = 1;
	public static final int INT_SIZE = 4;
	public static final int LONG_SIZE = 8;
	public static final int FLOAT_SIZE = 4;
	public static final int DOUBLE_SIZE = 8;
	public static final int CHAR_SIZE = 2;

	private final TaskType type;

	/**
	 * Basic Task or Message.
	 *
	 * @param type	Type of Task/Message to extract the correct type.
	 * @param name	Task name.
	 * @param flopsAmount	Processing amount (in flop) needed to process the task.
	 * @param bytesAmount	Transfer amount (in bytes) needed to transfer this task/message.
	 */
	public AbstractTask(TaskType type, String name, double flopsAmount, double bytesAmount) {
		super(name, flopsAmount, bytesAmount);
		this.type = type;
	}

	public TaskType getType() {
		return type;
	}
}
