package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.statistics.JobStatistics;
import at.enfilo.lbsim.model.statistics.WorkerStatistics;
import at.enfilo.lbsim.model.task.*;
import at.enfilo.lbsim.model.task.Task;
import org.simgrid.msg.*;

import java.util.*;

public class Worker extends BaseProcess {
	private static final double WORKER_THREAD_STARTUP_WAIT = 0.001;
	private static final double WORKER_THREAD_SHUTDOWN_WAIT = 0.01;
	private static final double STATE_INFO_PROVIDER_SHUTDOWN_WAIT = 0.01;

	private final HashMap<Integer, Queue<Task>> taskQueues;
	private final List<Integer> taskQueueOrder;
	private final List<WorkerThread> workerThreads;
	private StateInfoProvider stateInfoProvider;
	private String nearestDataProvider;
	private final Object queueLock = new Object();

	public Worker(Host host, String name, String[] args) {
		super(host, name, args);
		this.taskQueues = new HashMap<>();
		this.taskQueueOrder = new LinkedList<>();
		this.workerThreads = new LinkedList<>();
	}


	/**
	 * Setup process.
	 *
	 * @param args Arguments from deployment.xml
	 * @return Name of own Mailbox to listen. If null is returned no listen.
	 */
	@Override
	protected String setup(String[] args) {
		if (args.length != 2) {
			throw new RuntimeException(
					"Worker needs two arguments: " +
					"(1) number of worker threads and " +
					"(2) nearest data provider host.");
		}

		// Attach Worker <-> StateInfoProvider
		stateInfoProvider = StateInfoProvider.getInstance(getHost().getName());
		if (stateInfoProvider == null) {
			throw new RuntimeException("On every worker instance also StateInfoProvider instance must be running.");
		}
		stateInfoProvider.setWorker(this);

		// Nearest DataProvider
		nearestDataProvider = args[1];

		// Start worker threads
		int numberOfThreads = Integer.parseInt(args[0]);
		for (int i = 0; i < numberOfThreads; i++) {
			launchWorkerThread();
		}

		// Statistic
		WorkerStatistics.registerWorker(getHost().getName(), getHost().getSpeed(), (int)getHost().getCoreNumber());

		return ProcessHelper.constructMailboxName(Worker.class, getHost().getName());
	}


	/**
	 * Parse an incoming task / message.
	 *
	 * @param type Type of task.
	 * @param task Task.
	 */
	@Override
	protected void parseAbstractTask(TaskType type, AbstractTask task) {
		switch (type) {
			case TASK:
				Task t = Task.class.cast(task);
				queueTask(t, false);
				break;

			case ORDER:
				Order order = Order.class.cast(task);
				switch (order.getCmd()) {
					case MOVE_TASKS:
						// Payload format: '<taskId>[,<taskId>,...];<targetHost>'
						String[] payload = order.getPayload().split(";");
						List<Integer> taskIds = new LinkedList<>();
						for (String taskId : payload[0].split(",")) {
							taskIds.add(Integer.parseInt(taskId));
						}
						moveTasks(taskIds, payload[1]);
						break;

					case SEND_TASK:
						// One WorkerThread is idle, send next task
						if (listTasks().getNumberOfTasks() > 0) {
							notifyWorkerThreads();
						}
						break;

					default:
						throw new RuntimeException("Command not support: " + order.getCmd());
				}
				break;

			default:
				throw new RuntimeException("Unknown task received: " + task);
		}
	}


	/**
	 * Queue task to job a queue belongs to job id.
	 *
	 * @param task	Task to queue.
	 * @param subTask	True if task is a sub-task or a 'recursion task'
	 */
	void queueTask(Task task, boolean subTask) {
		Integer queueId = task.getJobId();
		if (!taskQueues.containsKey(queueId)) {
			taskQueues.put(queueId, new LinkedList<>());
			taskQueueOrder.add(queueId);
			taskQueueOrder.sort(Integer::compare);
		}
		SourceEvent event;
		if (subTask) {
			// Add to first
			((LinkedList<Task>)taskQueues.get(queueId)).addFirst(task);
			event = SourceEvent.INITIAL_QUEUE_SUB_TASK;
		} else {
			// Add to end
			taskQueues.get(queueId).add(task);
			event = SourceEvent.INITIAL_QUEUE_TASK;
		}

		if (task.getMoveCounter() > 0) {
			event = SourceEvent.QUEUE_MOVED_TASK;
		}

		// Statistics
		WorkerStatistics.updateQueueLength(getHost().getName(), queueId, taskQueues.get(queueId).size());
		JobStatistics.startQueueTask(task.getJobId(), task.getTaskId());
		// Notify all workers for new task
		notifyWorkerThreads();
		// Notify StateInfoProvider with actual queueLength
		updateStateInfoProvider(event);
	}


	/**
	 * Active (push) update to StateInfoProvider instance about queue length.
	 */
	private void updateStateInfoProvider(SourceEvent event) {
		TasksInfo ti = listTasks();
		stateInfoProvider.update(ti, event);
	}


	/**
	 * Move a given task to a new worker node.
	 *
	 * @param taskIds	Tasks to move.
	 * @param target	Target worker.
	 */
	private void moveTasks(List<Integer> taskIds, String target) {
		List<Task> tasksToMove = new LinkedList<>();

		// Iterate through all queues
		for (Queue<Task> queue : taskQueues.values()) {
			if (queue == null) {
				continue;
			}
			Iterator<Task> itQueued = queue.iterator();
			while (itQueued.hasNext() && taskIds.size() > 0) {
				Task t = itQueued.next();
				// Find task by id and send it to another worker node
				Iterator<Integer> itMove = taskIds.iterator();
				while (itMove.hasNext()) {
					int taskId = itMove.next();
					if (t.getTaskId() == taskId) {
						// Found, remove and send
						itQueued.remove();
						itMove.remove();
						//if (t.getRecursions() == 0) {
							// do not move a "root" recursion task
							tasksToMove.add(t);
						//}
						// Statistics
						WorkerStatistics.updateQueueLength(getHost().getName(), t.getJobId(), queue.size());
						break;
					}
				}
			}
		}

		for (Task t : tasksToMove) {
			Msg.verb("Moved Task " + t.getTaskId() + " (" + t.getOriginWorkerThread() + "): " + getHost().getName() + " --> " + target);
			t.increaseMoveCounter();
			try {
				t.send(ProcessHelper.constructMailboxName(Worker.class, target));
			} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
				e.printStackTrace();
			}
			// Statistic
			JobStatistics.moveTask(t.getJobId(), t.getTaskId());
		}

		// Notify StateInfoProvider with actual queueLength
		updateStateInfoProvider(SourceEvent.MOVE_TASK);
	}


	/**
	 * Generate {@link TasksInfo} message, which includes the number of queued tasks and the task ids.
	 *
	 * @return	TasksInfo object.
	 */
	TasksInfo listTasks() {
		List<Task> tasks = new LinkedList<>();
		for (Integer queueId : taskQueueOrder) {
			Queue<Task> queue = taskQueues.get(queueId);
			if (queue != null) {
				tasks.addAll(queue);
			}
		}

		return new TasksInfo(tasks);
	}


	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {
		try {
			// send shutdown command to worker threads
			int threads = workerThreads.size();
			for (int i = 0; i < threads; i++) {
				shutdownWorkerThread();
				waitFor(WORKER_THREAD_SHUTDOWN_WAIT);
			}

			Order shutdown = new Order(Command.SHUTDOWN);
			// send shutdown command to StateInfoProvider instance
			shutdown.send(ProcessHelper.constructMailboxName(StateInfoProvider.class, getHost().getName()));
			waitFor(STATE_INFO_PROVIDER_SHUTDOWN_WAIT);
			// send shutdown command to data provider
			if (DataProvider.runningOnHost(nearestDataProvider)) {
				shutdown.send(ProcessHelper.constructMailboxName(DataProvider.class, nearestDataProvider));
			}
			// send shutdown to load balancer processes on this worker, if there are
			if (LoadBalancer.runningOnHost(getHost().getName())) {
				shutdown.send(ProcessHelper.constructMailboxName(LoadBalancer.class, getHost().getName()));
			}
		} catch (HostFailureException | TransferFailureException | TimeoutException | NativeException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Send next task in queue to WorkerThreads.
	 */
	private void notifyWorkerThreads() {
		for (WorkerThread wt : workerThreads) {
			if (wt.isIdle()) {
				// If one worker thread is idle, send notification
				Order fetchNext = new Order(Command.FETCH_TASK);
				try {
					wt.setBusy();
					// Could be sync, because at least one thread is idle
					fetchNext.send(ProcessHelper.constructMailboxName(WorkerThread.class, getHost().getName()));
				} catch (TransferFailureException | HostFailureException | TimeoutException | NativeException e) {
					e.printStackTrace();
				}
				break;
			}
		}
	}


	/**
	 * Fetch next task in queue with lowest priority and return it.
	 *
	 * @return	Next task to process.
	 */
	Task nextTask() {
		Task t = null;
		synchronized (queueLock) {
			for (Integer queueId : taskQueueOrder) {
				Queue<Task> queue = taskQueues.get(queueId);
				if (!queue.isEmpty()) {
					t = queue.poll();
					break;
				}
			}
		}
		if (t != null) {
			// Statistics
			WorkerStatistics.updateQueueLength(getHost().getName(), t.getJobId(), taskQueues.get(t.getJobId()).size());
			JobStatistics.endQueueTask(t.getJobId(), t.getTaskId());
			// Notify StateInfoProvider with actual queueLength
			updateStateInfoProvider(SourceEvent.FETCH_NEXT_TASK);
		}
		return t;
	}


	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}


	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() { }


	/**
	 * Returns the nearest data provider host of this worker.
	 * @return	Hostname of data provider host.
	 */
	String getNearestDataProvider() {
		return nearestDataProvider;
	}


	/**
	 * Launch a new worker thread
	 */
	void launchWorkerThread() {
		WorkerThread wt = new WorkerThread(this, getHost(), WorkerThread.class.getName(), new String[]{});
		try {
			wt.start();
			workerThreads.add(wt);
			waitFor(WORKER_THREAD_STARTUP_WAIT); // Wait for thread to startup
		} catch (HostNotFoundException | HostFailureException e) {
			e.printStackTrace();
		}

	}


	/**
	 * Shutdown next idle WorkerThread.
	 */
	void shutdownWorkerThread() {
		if (workerThreads.size() > 0) {
			Order shutdown = new Order(Command.SHUTDOWN);
			try {
				shutdown.send(ProcessHelper.constructMailboxName(WorkerThread.class, getHost().getName()));
			} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
				e.printStackTrace();
			}
		}
	}


	/**
	 * Deregister a WorkerThread.
	 * @param workerThread	Thread to deregister
	 */
	void deregister(WorkerThread workerThread) {
		workerThreads.remove(workerThread);
	}
}
