package at.enfilo.lbsim.model.process;


import at.enfilo.lbsim.model.task.Command;
import at.enfilo.lbsim.model.task.Order;
import org.simgrid.msg.*;
import org.simgrid.msg.Process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

public class Client extends Process {
	private String brokerMailbox;
	private final Queue<ClientCommand> scenarioQueue;
	private final LinkedList<ClientCommand> scenarioList;

	public Client(Host host, String name, String[] args) {
		super(host, name, args);
		scenarioList = new LinkedList<>();
		scenarioQueue = scenarioList;
	}
	
	@Override
	public void main(String[] args) throws MsgException {
		if (args.length != 2) {
			throw new RuntimeException("Client Process need two arguments: master-node and job/task file.");
		}

		// First argument: scenarioQueue file
		File scenarioFile = new File(args[0]);

		// Second argument: broker hostname
		brokerMailbox = ProcessHelper.constructMailboxName(Broker.class, args[1]);

		readScenario(scenarioFile);
		startScenario();
	}

	/**
	 * Reads scenarioQueue from file and build client queue.
	 * @param scenarioFile
	 */
	private void readScenario(File scenarioFile) {
		boolean firstJob = true;
		try (BufferedReader reader = new BufferedReader(new FileReader(scenarioFile))) {
			String line;
			while ((line = reader.readLine()) != null) {
				// Fields:
				//	0:	type (j=job, t=task, s=sub-task)
				//	1:	time to wait
				//	2:	computation duration (flops)
				//	3:	task-size (bytes)
				//	4:	additional required data (bytes)
				//	5:	result size (bytes)
				//	6:	recursions (optional, default = 0)

				line = line.trim();
				if (line.startsWith("#")) {
					// comment found
					continue;
				}

				// Split by ';'
				String[] fields = line.split(";");
				if (fields.length < 2) {
					continue;
				}

				double waitFor = Double.parseDouble(fields[1]);
				switch (fields[0]) {
					// JobData
					case "j":
						if (!firstJob) {
							scenarioQueue.add(new ClientCommand(0, new Order(Command.SUBMIT_JOB)));
						}
						scenarioQueue.add(new ClientCommand(waitFor, new Order(Command.CREATE_JOB)));
						firstJob = false;
						break;

					// Task
					case "t":
						double taskSize = Double.parseDouble(fields[3]);
						String recursions = fields.length == 7 ? fields[6] : "0"; // Default: no recursions
						String payload = fields[2] + ";" + fields[3] + ";" + fields[4] + ";" + fields[5] + ";" + recursions;
						Order createTask = new Order(Command.CREATE_TASK, payload, taskSize);
						scenarioQueue.add(new ClientCommand(waitFor, createTask));
						break;

					// Sub-Task
					case "s":
						String subRecursions = fields.length == 7 ? fields[6] : "0"; // Default: no recursions
						String subPayload = fields[2] + ";" + fields[3] + ";" + fields[4] + ";" + fields[5] + ";" + subRecursions;
						scenarioList.getLast().getOrder().extendPayload(subPayload);
						break;

					// Unknown
					default:
						throw new RuntimeException("Error while parsing scenarioQueue: First field must be either 't' or 'j'.");
				}
			}

			scenarioQueue.add(new ClientCommand(0, new Order(Command.SUBMIT_JOB)));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Start to proceed the scenario.
	 *
	 * @throws HostFailureException
	 */
	private void startScenario() throws HostFailureException {
		// Go through scenario - command by command
		for (ClientCommand cc : scenarioQueue) {
			if (cc.getWaitFor() > 0) {
				waitFor(cc.getWaitFor());
			}
			try {
				cc.getOrder().send(brokerMailbox);
			} catch (TransferFailureException | TimeoutException | NativeException e) {
				e.printStackTrace();
			}
		}

		// Scenario done, send shutdown
		Order shutdown = new Order(Command.SHUTDOWN);
		try {
			shutdown.send(brokerMailbox);
		} catch (TransferFailureException | TimeoutException | NativeException e) {
			e.printStackTrace();
		}
	}
}
