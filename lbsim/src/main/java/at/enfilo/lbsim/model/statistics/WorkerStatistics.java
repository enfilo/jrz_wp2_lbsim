package at.enfilo.lbsim.model.statistics;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.stream.Collectors;


public class WorkerStatistics {
	public static final String DEFAULT_QUEUE_FILE_NAME = "worker_queue_stats.csv";
	public static final String DEFAULT_WORKER_FILE_NAME = "worker_stats.csv";

	private static final HashMap<String, WorkerData> workers = new HashMap<>();


	public static void registerWorker(String wokerHost, double speed, int cores) {
		WorkerData wd = new WorkerData(wokerHost, speed, cores);
		workers.put(wokerHost, wd);
	}

	public static void updateQueueLength(String workerHost, int queueId, int length) {
		if (!workers.containsKey(workerHost)) {
			workers.put(workerHost, new WorkerData(workerHost));
		}
		WorkerDataItem wdi = new WorkerDataItem(queueId, length);
		workers.get(workerHost).addWorkerDataItem(wdi);
	}


	public static void processedTask(String workerHost, double flopsProcessed) {
		if (!workers.containsKey(workerHost)) {
			workers.put(workerHost, new WorkerData(workerHost));
		}
		workers.get(workerHost).processedTask(flopsProcessed);
	}


	public static void writeStatistics(String workerReportFile, String queueReportFile) {
		// Worker writeStatistics
		try (CSVWriter writer = new CSVWriter(new FileWriter(workerReportFile))) {
			String[] title = new String[]{"Worker", "Speed", "Cores", "ProcessedTasks", "ProcessedFlops"};
			writer.writeNext(title);
			for (WorkerData wd : workers.values().stream().sorted((wd1, wd2) -> wd1.getHostname().compareTo(wd2.getHostname())).collect(Collectors.toList())) {
				String[] entries = new String[title.length];
				int i = 0;
				entries[i++] = wd.getHostname();
				entries[i++] = Double.toString(wd.getSpeed());
				entries[i++] = Integer.toString(wd.getCores());
				entries[i++] = Integer.toString(wd.getTaskProcessed());
				entries[i++] = Double.toString(wd.getFlopsProcessed());
				writer.writeNext(entries);
			}
			writer.flush();
			//writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Queue history writeStatistics
		try (CSVWriter writer = new CSVWriter(new FileWriter(queueReportFile))) {
			String[] title = new String[]{"Worker", "Timestamp", "Queue", "Length"};
			writer.writeNext(title);
			for (WorkerData wd : workers.values().stream().sorted((wd1, wd2) -> wd1.getHostname().compareTo(wd2.getHostname())).collect(Collectors.toList())) {
				for (WorkerDataItem wdi : wd.getHistory()) {
					String[] entries = new String[title.length];
					int i = 0;
					entries[i++] = wd.getHostname();
					entries[i++] = Double.toString(wdi.getTimestamp());
					entries[i++] = Integer.toString(wdi.getQueueId());
					entries[i++] = Integer.toString(wdi.getLenght());
 					writer.writeNext(entries);
				}
			}
			writer.flush();
			//writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void writeStatistics() {
		writeStatistics(DEFAULT_WORKER_FILE_NAME, DEFAULT_QUEUE_FILE_NAME);
	}
}
