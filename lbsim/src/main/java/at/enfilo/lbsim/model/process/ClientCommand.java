package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.task.Order;

class ClientCommand {
	private final double waitFor;
	private final Order order;

	public ClientCommand(double waitFor, Order order) {
		this.waitFor = waitFor;
		this.order = order;
	}

	public double getWaitFor() {
		return waitFor;
	}

	public Order getOrder() {
		return order;
	}
}
