package at.enfilo.lbsim.model.task;

public enum Metric {
	QUEUE_LENGTH_ALL_JOBS, // Queue length over all active jobs
	QUEUE_LENGTH, // Queue length for active job with highest priority
	LOAD,
	CPU_CORES,
	CPU_SPEED,
	RUNNING_THREADS
}
