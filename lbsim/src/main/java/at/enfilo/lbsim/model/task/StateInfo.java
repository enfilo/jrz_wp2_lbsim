package at.enfilo.lbsim.model.task;

public class StateInfo extends AbstractTask {
	private static final String NAME_PREFIX = "StateInfo_";
	private static long idCounter = 0;

	private final double[] metricValues = new double[Metric.values().length];

	public StateInfo() {
		super(TaskType.STATE_INFO, NAME_PREFIX + (idCounter++), 0, (Metric.values().length * AbstractTask.DOUBLE_SIZE));
	}

	public double getMetricValue(Metric m) {
		return metricValues[m.ordinal()];
	}

	public void setMetricValue(Metric m, double value) {
		metricValues[m.ordinal()] = value;
	}

}
