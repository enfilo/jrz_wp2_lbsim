package at.enfilo.lbsim.model.process;

public enum SourceEvent {
	INITIAL_QUEUE_SUB_TASK,
	INITIAL_QUEUE_TASK,
	FETCH_NEXT_TASK,
	MOVE_TASK,
	QUEUE_MOVED_TASK
}
