package at.enfilo.lbsim.model.statistics;

/**
 * Message Data
 */
class MsgData {
	private final double timeStamp;
	private final String node;
	private final String process;
	private final String direction; // RCV, SND
	private final String type;
	private final double size;

	public MsgData(double timeStamp, String node, String process, String type, String direction, double size) {
		this.timeStamp = timeStamp;
		this.node = node;
		this.process = process;
		this.direction = direction;
		this.type = type;
		this.size = size;
	}

	double getTimeStamp() {
		return timeStamp;
	}

	String getNode() {
		return node;
	}

	String getType() {
		return type;
	}

	double getSize() {
		return size;
	}

	String getProcess() {
		return process;
	}

	String getDirection() {
		return direction;
	}
}
