package at.enfilo.lbsim.model.task;

import at.enfilo.lbsim.model.process.JobFinishedObserver;
import at.enfilo.lbsim.model.statistics.JobStatistics;
import org.simgrid.msg.*;

import java.util.*;

public class Job {
	private static int idCounter = 0;
	private static int highestActivePrio = idCounter;

	private final int id;
	private int activeTasks;
	private final HashMap<Integer, Task> tasks;
	private final List<JobFinishedObserver> observers;
	private final Object lock = new Object();

	public Job() {
		this.id = idCounter++;
		this.activeTasks = 0;
		this.observers = new LinkedList<>();
		this.tasks = new HashMap<>();
	}

	public void addTask(Task t) {
		if (!tasks.containsKey(t.getTaskId())) {
			activeTasks++;
			tasks.put(t.getTaskId(), t);
		}
	}

	public void taskFinished(Task t) {
		synchronized (lock) {
			activeTasks--;
		}
		if (activeTasks == 0) {
			// no active task --> job is done
			jobFinished();
		}
	}

	public void registerObserver(JobFinishedObserver notifier) {
		observers.add(notifier);
	}

	public int getId() {
		return id;
	}

	public static int getHighestActivePrio() {
		return highestActivePrio;
	}

	private void jobFinished() {
		//Msg.info("Execution of Job " +  highestActivePrio + " done, fetching results.");
		// Collect all task results
		// --> To emulate the current DEF System.
		/*
		for (Task t : tasks.values()) {
			// Format of payload: '<mailbox>;<amountOfBytes>'
			String mailbox = UUID.randomUUID().toString();
			String payload = mailbox + ";" + t.getResultData();
			Order fetchResult = new Order(Command.REQUEST_DATA, payload);
			try {
				fetchResult.send(ProcessHelper.constructMailboxName(DataProvider.class, "master"));
				Task.receive(mailbox);
			} catch (TimeoutException | HostFailureException | NativeException | TransferFailureException e) {
				e.printStackTrace();
			}
		}
		*/
		Msg.info("Job " + highestActivePrio + " done.");
		// Statistics
		JobStatistics.finishedJob(highestActivePrio);

		for (JobFinishedObserver observer : observers) {
			observer.notifyFinished(this);
		}
		highestActivePrio++;
	}
}
