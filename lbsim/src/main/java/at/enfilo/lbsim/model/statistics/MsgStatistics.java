package at.enfilo.lbsim.model.statistics;

import com.opencsv.CSVWriter;
import org.simgrid.msg.Msg;

import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class MsgStatistics {
	private static final String SEND = "SND";
	private static final String RECEIVE = "RCV";
	private final static List<MsgData> data = new LinkedList<>();

	public static final String DEFAULT_MSG_FILE = "msg_stats.csv";


	public static void sendMessage(String host, String process, String type, double size) {
		data.add(new MsgData(Msg.getClock(), host, process, type, SEND, size));
	}

	public static void receivedMessage(String host, String process, String type, double size) {
		data.add(new MsgData(Msg.getClock(), host, process, type, RECEIVE, size));
	}

	public static void writeStatistics(String file) {
		// Write job statistics
		try (CSVWriter writer = new CSVWriter(new FileWriter(file))) {
			String[] title = new String[]{
					"timestamp", "node", "process", "type", "direction", "size"
			};
			writer.writeNext(title);
			for (MsgData md : data) {
				String[] entries = new String[title.length];
				int i = 0;
				entries[i++] = Double.toString(md.getTimeStamp());
				entries[i++] = md.getNode();
				entries[i++] = md.getProcess();
				entries[i++] = md.getType();
				entries[i++] = md.getDirection();
				entries[i++] = Double.toString(md.getSize());
				writer.writeNext(entries);
			}
			writer.flush();
			//writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeStatistics() {
		writeStatistics(DEFAULT_MSG_FILE);
	}
}
