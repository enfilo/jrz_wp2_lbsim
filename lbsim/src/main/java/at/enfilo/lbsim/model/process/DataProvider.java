package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.task.AbstractTask;
import at.enfilo.lbsim.model.task.Data;
import at.enfilo.lbsim.model.task.Order;
import at.enfilo.lbsim.model.task.TaskType;
import org.simgrid.msg.Host;

import java.util.LinkedList;
import java.util.List;


public class DataProvider extends BaseProcess {

	private static final List<String> hosts = new LinkedList<>();


	public DataProvider(Host host, String name, String[] args) {
		super(host, name, args);
		hosts.add(host.getName());
	}


	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}


	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() { }


	/**
	 * Setup process.
	 *
	 * @param args Arguments from deployment.xml
	 * @return Name of own Mailbox to listen. If null is returned no listen.
	 */
	@Override
	protected String setup(String[] args) {
		return ProcessHelper.constructMailboxName(this.getClass(), getHost().getName());
	}


	/**
	 * Parse an incoming task / message.
	 *
	 * @param type Type of task.
	 * @param task Task.
	 */
	@Override
	protected void parseAbstractTask(TaskType type, AbstractTask task) {
		switch (type) {
			case DATA:
				Data data = Data.class.cast(task);
				store(data);
				break;

			case ORDER:
				Order order = Order.class.cast(task);
				switch (order.getCmd()) {
					case REQUEST_DATA:
						// Format of payload: '<mailbox>;<amountOfBytes>'
						String[] payload = order.getPayload().split(";");
						String mailbox = payload[0];
						double bytes = Double.parseDouble(payload[1]);
						sendData(mailbox, bytes);
						break;

					default:
						throw new RuntimeException("Command not support: " + order.getCmd());
				}
				break;

			default:
				throw new RuntimeException("Unknown task received: " + task);
		}
	}


	/**
	 * Send requested data to specified mailbox.
	 *
	 * @param mailbox	Receiver mailbox for the data.
	 * @param bytes		How many bytes to send.
	 */
	private void sendData(String mailbox, double bytes) {
		Data data = new Data(bytes);
		data.dsend(mailbox); // Send async data
		// --> incoming request is serial, but sending back data is parallel
	}


	/**
	 * Store data.
	 * @param data	Data to store.
	 */
	private void store(Data data) {
		// Do nothing?
	}


	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {
	}


	/**
	 * Returns true if a DataProvider process running on the given hostName.
	 *
	 * @param hostName	hostname to prove.
	 * @return
	 */
	static boolean runningOnHost(String hostName) {
		boolean running = hosts.contains(hostName);
		if (running) {
			hosts.remove(hostName);
		}
		return running;
	}

}
