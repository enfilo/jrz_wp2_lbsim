package at.enfilo.lbsim.model.process;

public class ProcessHelper {

	public static String constructMailboxName(Class c, String hostname) {
		String prefix = c.getSimpleName();
		if (InitialScheduler.class.isAssignableFrom(c)) {
			prefix = InitialScheduler.class.getSimpleName();
		}
		if (StateInfoProvider.class.isAssignableFrom(c)) {
			prefix = StateInfoProvider.class.getSimpleName();
		}
		if (LoadBalancer.class.isAssignableFrom(c)) {
			prefix = LoadBalancer.class.getSimpleName();
		}
		return prefix + "_" + hostname;
	}

}
