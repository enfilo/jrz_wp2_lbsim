package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.statistics.MsgStatistics;
import at.enfilo.lbsim.model.task.*;
import at.enfilo.lbsim.model.task.Task;
import org.simgrid.msg.*;

import java.util.LinkedList;
import java.util.Queue;

public abstract class InitialScheduler extends BaseProcess {
	private static final double WAIT_BETWEEN_SHUTDOWN_MSGS = 0.01;
	private static final double WAIT_FOR_PERIODIC_PROCESS = 1.0;

	protected String[] neighbors;
	protected boolean waitForSubmit;
	protected final Queue<Task> tasks;

	public InitialScheduler(Host host, String name, String[] args) {
		super(host, name, args);
		tasks = new LinkedList<>();
	}

	/**
	 * Setup process.
	 *
	 * @param args Arguments from deployment.xml
	 * @return Name of own Mailbox to listen. If null is returned no listen.
	 */
	@Override
	protected String setup(String[] args) {
		if (args.length < 2) {
			throw new RuntimeException("InitialScheduler needs at least two arguments: neighbors and wait-for-submit-flag");
		}

		// Neighbor (worker) list is comma separated
		neighbors = args[0].split(",");
		waitForSubmit = Boolean.parseBoolean(args[1]);

		if (args.length > 2) {
			String[] furtherArgs = new String[args.length - 2];
			System.arraycopy(args, 2, furtherArgs, 0, args.length - 2);
			setupFurtherArgs(furtherArgs);
		}

		// Initialize concrete Initial-Scheduler
		init();

		return ProcessHelper.constructMailboxName(this.getClass(), getHost().getName());
	}


	/**
	 * Initialize scheduler. Will be called after setupFurtherArgs and before start listening.
	 */
	protected abstract void init();


	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args	Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *              already parsed and pruned.
	 */
	protected abstract void setupFurtherArgs(String[] args);


	/**
	 * Parse an incoming task / message.
	 *
	 * @param type Type of task.
	 * @param task Task.
	 */
	@Override
	protected void parseAbstractTask(TaskType type, AbstractTask task) {
		// Statistics
		MsgStatistics.receivedMessage(getHost().getName(), this.getClass().getName(), type.name(), task.getMessageSize());

		switch (type) {
			case ORDER:
				Order order = Order.class.cast(task);
				switch (order.getCmd()) {
					case SUBMIT_JOB:
						Msg.verb("Submit job received. Scheduling all queued tasks");
						prepareForSubmit(tasks.size());
						while (!tasks.isEmpty()) {
							scheduleTask(tasks.poll());
						}
						break;

					default:
						throw new RuntimeException("Command not support: " + order.getCmd());
				}
				break;

			case TASK:
				Task t = Task.class.cast(task);
				if (waitForSubmit) {
					Msg.verb("Received task for scheduling: queuing.");
					tasks.add(t);
				} else {
					Msg.verb("Received task for scheduling: instant scheduling.");
					scheduleTask(t);
				}
				break;

			case STATE_INFO:
				StateInfo si = StateInfo.class.cast(task);
				parseStateInfo(si);
				break;

			default:
				throw new RuntimeException("Unknown task received: " + task);
		}
	}


	/**
	 * This method is only performed if {@link this.waitForSubmit} flag is true.
	 *
	 * Method is called while submitting job and before start scheduling tasks by selecting workers.
	 * After this method {@method selectWorker()} is called for every task in queue.
	 *
	 * @param numberOfTasks	Number of tasks to schedule.
	 */
	protected abstract void prepareForSubmit(int numberOfTasks);


	/**
	 * Handle incoming StateInfo task/message.
	 *
	 * @param si	StateInfo to handle.
	 */
	protected abstract void parseStateInfo(StateInfo si);


	/**
	 * Schedule a task to a worker node.
	 *
	 * @param t 	Task to schedule.
	 */
	protected void scheduleTask(Task t) {
		String worker = selectWorker();
		String workerMailbox = ProcessHelper.constructMailboxName(Worker.class, worker);
		//t.dsend(workerMailbox); // Asynchronous send
		try {
			t.send(workerMailbox);
		} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Select next worker to schedule task to it.
	 *
	 * @return	Hostname of worker.
	 */
	protected abstract String selectWorker();


	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {
		try {
			try {
				PeriodicProcess.shutdown();
				while (PeriodicProcess.countRunningPeriodicProcesses() > 0) {
					waitFor(WAIT_FOR_PERIODIC_PROCESS);
					Msg.verb("Waiting for all PeriodicProcesses...");
				}
			} catch (HostFailureException e) {
				e.printStackTrace();
			}
			// send shutdown to load balancer processes on this host, if there are
			if (LoadBalancer.runningOnHost(getHost().getName())) {
				Order shutdown = new Order(Command.SHUTDOWN);
				shutdown.send(ProcessHelper.constructMailboxName(LoadBalancer.class, getHost().getName()));
			}
			waitFor(WAIT_BETWEEN_SHUTDOWN_MSGS);

			// send shutdown to workers
			for (String worker : neighbors) {
				Order shutdown = new Order(Command.SHUTDOWN);
				shutdown.send(ProcessHelper.constructMailboxName(Worker.class, worker));
				waitFor(WAIT_BETWEEN_SHUTDOWN_MSGS);
			}

		} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
			e.printStackTrace();
		}
	}
}
