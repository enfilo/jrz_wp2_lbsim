package at.enfilo.lbsim.model.task;

import at.enfilo.lbsim.model.process.WorkerThread;

import java.util.LinkedList;
import java.util.List;

public class Task extends AbstractTask {
	private static final String NAME_PREFIX = "Task_";
	private static int taskIdCounter = 0;

	private final int taskId;
	private final Job job;
	private final double requiredData; // required data from DataProvider
	private int recursions; // how many recursions
	private final double resultData; // result data to store on DataProvider
	private WorkerThread originWorkerThread; // origin worker stack (start recursion or sub task)
	private final List<Task> subTasks;
	private double computeDuration;
	private Task recursionCaller;
	private int moveCounter;

	/**
	 * Creates a executable Task.
	 * @param job job to which this task belong
	 * @param computeDuration how long takes the execution of this task
	 * @param taskSize how many data is directly attached to this task (call by value data)
	 * @param requiredData data (call by reference) for this task, fetched from DataProvider
	 * @param resultData result data to store on DataProvider
	 */
	public Task(Job job, double computeDuration, double taskSize, double requiredData, double resultData) {
		this(job, computeDuration, taskSize, requiredData, resultData, 0);
	}

	/**
	 * Creates a executable Task.
	 *
	 * @param job             job to which this task belong
	 * @param computeDuration how long takes the execution of this task
	 * @param taskSize        how many data is directly attached to this task (call by value data)
	 * @param requiredData    data (call by reference) for this task, fetched from DataProvider
	 * @param resultData      result data to store on DataProvider
	 * @param recursions      how often this task will be called in a recursion
	 */
	public Task(Job job, double computeDuration, double taskSize, double requiredData, double resultData, int recursions) {
		super(TaskType.TASK, NAME_PREFIX + taskIdCounter, computeDuration, taskSize);
		this.taskId = taskIdCounter++;
		this.job = job;
		this.requiredData = requiredData;
		this.resultData = resultData;
		this.recursions = recursions;
		this.subTasks = new LinkedList<>();
		this.computeDuration = computeDuration;
		job.addTask(this);
	}

	public int getTaskId() {
		return taskId;
	}

	public int getJobId() {
		return job.getId();
	}

	public double getRequiredData() {
		return requiredData;
	}

	public int getRecursions() {
		return recursions;
	}

	public double getResultData() {
		return resultData;
	}

	/**
	 * Returns WorkerThread who started this task.
	 * @return	WorkerThread instance.
	 */
	public WorkerThread getOriginWorkerThread() {
		return originWorkerThread;
	}

	public Task cloneForRecursion(WorkerThread workerThread) {
		Task clone = new Task(job, computeDuration, getMessageSize(), requiredData, resultData, (recursions - 1));
		clone.originWorkerThread = workerThread;
		clone.recursionCaller = this;
		for (Task sub : subTasks) {
			Task subClone = sub.cloneForRecursion(null);
			if (sub.recursions > 0) {
				subClone.recursions++;
			}
			clone.subTasks.add(subClone);
		}
		return clone;
	}

	public void addSubTask(Task task) {
		subTasks.add(task);
	}

	public boolean hasSubTasks() {
		return subTasks.size() > 0;
	}

	public List<Task> getSubTasks() {
		return subTasks;
	}

	public void setOriginWorkerThread(WorkerThread originWorkerThread) {
		this.originWorkerThread = originWorkerThread;
	}

	public double getComputeDuration() {
		return computeDuration;
	}

	public void finished() {
		job.taskFinished(this);
	}


	/**
	 * Get task id from root task of recursion.
	 * @return task id from froot task.
	 */
	public int getRootOfRecursion() {
		int id = taskId;
		Task t = recursionCaller;
		while (t != null) {
			id = t.getTaskId();
			t = t.recursionCaller;
		}
		return id;
	}


	/**
	 * Increases the move counter
	 */
	public void increaseMoveCounter() {
		moveCounter++;
	}


	public int getMoveCounter() {
		return moveCounter;
	}
}
