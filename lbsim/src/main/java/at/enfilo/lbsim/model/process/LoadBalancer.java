package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.statistics.MsgStatistics;
import at.enfilo.lbsim.model.task.*;
import org.simgrid.msg.*;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


public abstract class LoadBalancer extends BaseProcess {

	private static final List<String> hosts = new LinkedList<>();

	/**
	 * Constructs a new process from a host and his name, the arguments of here method function are
	 * specified by the parameter args.
	 *
	 * @param host The host of the process to create.
	 * @param name The name of the process.
	 * @param args The arguments of main method of the process.
	 */
	public LoadBalancer(Host host, String name, String[] args) {
		super(host, name, args);
		hosts.add(host.getName());
	}


	/**
	 * Setup process.
	 *
	 * @param args Arguments from deployment.xml
	 * @return Name of own Mailbox to listen. If null is returned no listen.
	 */
	@Override
	protected String setup(String[] args) {
		setupFurtherArgs(args);

		// Initialize concrete Initial-Scheduler
		init();

		return ProcessHelper.constructMailboxName(this.getClass(), getHost().getName());
	}


	/**
	 * Parse an incoming task / message.
	 *
	 * @param type Type of task.
	 * @param task Task.
	 */
	@Override
	protected void parseAbstractTask(TaskType type, AbstractTask task) {
		// Statistics
		MsgStatistics.receivedMessage(getHost().getName(), this.getClass().getName(), type.name(), task.getMessageSize());

		switch (type) {
			case STATE_INFO:
				StateInfo stateInfo = StateInfo.class.cast(task);
				if (parseIncomingStateInfo(stateInfo)) {
					runWorkflow();
				}
				break;

			case CUSTOM:
				if (parseIncomingCustomMessage(task)) {
					runWorkflow();
				}
				break;

			default:
				throw new RuntimeException("Received task type is not supported: " + task);
		}
	}


	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args	Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *              already parsed and pruned.
	 */
	protected abstract void setupFurtherArgs(String[] args);


	/**
	 * Initialize scheduler. Will be called after setupFurtherArgs and before start listening.
	 */
	protected abstract void init();


	/**
	 * Received a message / task, type CUSTOM, from another process.
	 * This method is called by receiving to parse the message.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 	1. {@link #estimateWorkers()}
	 * 	2. {@link #needOfBalance()}
	 * 	3. {@link #selectSource()}
	 * 	4. {@link #selectTasks(String)}
	 * 	5. {@link #selectTarget()}
	 * 	6. {@link #cleanupAfterBalance()}
	 *
	 * @param custom	Message to parse.
	 * @return	true: load balancing workflow will be started.
	 * 			false: Just parse the message.
	 */
	protected abstract boolean parseIncomingCustomMessage(AbstractTask custom);


	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() { }


	/**
	 * Run LoadBalancing WorkFlow:
	 *
	 * 	1. Information Strategy: Estimate worker by a new state information
	 * 	2. Decide to (re)balance or not and how many actions (x) needed
	 * 	3. If (re)balance then call the following steps x times
	 * 	3.1. Select source worker
	 * 	3.2. Select tasks to move
	 * 	3.3. Select target worker
	 * 	3.4. Move tasks
	 * 	4. Cleanup
	 */
	private void runWorkflow() {
		estimateWorkers(); // 1. Estimate workers
		int numberOfBalances = needOfBalance(); // 2. Check if (re)balance is needed and how many actions are necessary
		for (int i = 0; i < numberOfBalances; i++) {
			String source = selectSource(); // 3.1 Select Source
			if (source == null) {
				break;
			}
			List<Integer> tasks = selectTasks(source); // 3.2 Select Tasks
			if (tasks == null || tasks.size() == 0) {
				break;
			}
			String target = selectTarget(); // 3.3 Select Target
			if (target == null) {
				break;
			}

			// Send Move-Task orders
			// Payload format: '<taskId>[,<taskId>....];<targetHost>'
			String taskIds = tasks.stream().map(number -> String.valueOf(number)).collect(Collectors.joining(","));
			String payload = taskIds + ";" + target;
			Order moveTasksOrder = new Order(Command.MOVE_TASKS, payload);

			// Statistics
			MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), moveTasksOrder.getType().name(), moveTasksOrder.getMessageSize());

			moveTasksOrder.dsend(ProcessHelper.constructMailboxName(Worker.class, source)); // 3.4 Move Tasks
			Msg.verb("LoadBalancing Action: " + source + " --> " + target + " : " + taskIds);
		}
		cleanupAfterBalance(); // 4. Cleanup
	}


	/**
	 * Method will be called after advising selected workers to balance load (tasks).
	 * Its the last step of LoadBalancing Workflow to clean up all temporary information.
	 */
	protected abstract void cleanupAfterBalance();


	/**
	 * Estimate workers. Information received with {@link #parseIncomingCustomMessage(AbstractTask)} or
	 * {@link #parseIncomingStateInfo(StateInfo)}.
	 *
	 * This method is the first step of the LoadBalancing Workflow. Next step: {@link #needOfBalance()}.
	 */
	protected abstract void estimateWorkers();


	/**
	 * New StateInformation received. Parse and estimate Worker.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 	1. {@link #estimateWorkers()}
	 * 	2. {@link #needOfBalance()}
	 * 	3. {@link #selectSource()}
	 * 	4. {@link #selectTasks(String)}
	 * 	5. {@link #selectTarget()}
	 *  6. {@link #cleanupAfterBalance()}
	 *
	 * @param stateInfo	new StateInfo to parse
	 * @return	true: LoadBalancing workflow will be started
	 * 			false: Just parse the message
	 */
	protected abstract boolean parseIncomingStateInfo(StateInfo stateInfo);


	/**
	 * Returns how many (re)balance actions are necessary.
	 *
	 * @return	x  > 0: workflow will go ahead and call selectSource(), selectTasks() and selectTarget() x times.
	 *			x <= 0: workflow will be stopped
	 */
	protected abstract int needOfBalance();


	/**
	 * Select a target worker to move the selected task.
	 *
	 * @return	hostname of target worker.
	 */
	protected abstract String selectTarget();


	/**
	 * Returns a list of task ids to move.
	 *
	 * @param source	Source worker hostname.
	 * @return	List of Tasks to move.
	 */
	protected abstract List<Integer> selectTasks(String source);


	/**
	 * Returns the source (overloaded) worker.
	 *
	 * @return	Hostname of worker.
	 */
	protected abstract String selectSource();


	/**
	 * Returns {@link at.enfilo.lbsim.model.task.TasksInfo}, a list of all tasks, from the given worker host.
	 *
	 * @param workerHost	To request the TasksInfo
	 * @return	TasksInfo object.
	 * @throws HostFailureException
	 * @throws NativeException
	 * @throws TimeoutException
	 * @throws TransferFailureException
	 */
	protected TasksInfo requestTasksInfo(String workerHost)
			throws HostFailureException, NativeException, TimeoutException, TransferFailureException {

		String mailbox = UUID.randomUUID().toString();
		Order requestTasks = new Order(Command.LIST_TASKS, mailbox);

		// Statistics
		MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), requestTasks.getType().name(), requestTasks.getMessageSize());

		requestTasks.send(ProcessHelper.constructMailboxName(StateInfoProvider.class, workerHost));
		AbstractTask task = AbstractTask.class.cast(TasksInfo.receive(mailbox));
		if (task.getType() == TaskType.TASKS_INFO) {
			return TasksInfo.class.cast(task);
		}
		return null;
	}


	/**
	 * Returns true if a DataProvider process running on the given hostName.
	 *
	 * @param hostName	hostname to prove.
	 * @return
	 */
	static boolean runningOnHost(String hostName) {
		boolean running = hosts.contains(hostName);
		if (running) {
			hosts.remove(hostName);
		}
		return running;
	}


	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() {
		if (lbActivator()) {
			runWorkflow();
		}
	}


	/**
	 * Method lbActivator (LoadBalancing Activator) will be called every "interval" seconds.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 	1. {@link #estimateWorkers()}
	 * 	2. {@link #needOfBalance()}
	 * 	3. {@link #selectSource()}
	 * 	4. {@link #selectTasks(String)}
	 *  5. {@link #selectTarget()}
	 *  6. {@link #cleanupAfterBalance()}
	 *
	 * @return	true: LoadBalancing workflow will be started
	 * 			false: Just parse the message
	 */
	protected abstract boolean lbActivator();
}
