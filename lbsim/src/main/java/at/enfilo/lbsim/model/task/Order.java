package at.enfilo.lbsim.model.task;

/**
 * An order represent an command with data (payload).
 * Its a message, or task in view of SimGrid, to send from one process to other.
 */
public class Order extends AbstractTask {
	private static final String NAME_PREFIX = "Order_";
	private static long idCounter = 0;

	private final Command cmd;
	private final StringBuilder payload;

	public Order(Command cmd) {
		this(cmd, "");
	}

	public Order(Command cmd, String payload) {
		this(cmd, payload, (AbstractTask.CHAR_SIZE * payload.length() + AbstractTask.BYTE_SIZE));
	}

	public Order(Command cmd, String payload, double dataSize) {
		super(TaskType.ORDER, NAME_PREFIX + (idCounter++), 0, dataSize);
		this.cmd = cmd;
		this.payload = new StringBuilder(payload);
	}

	public Command getCmd() {
		return cmd;
	}

	public String getPayload() {
		return payload.toString();
	}

	public static long getIdCounter() {
		return idCounter;
	}

	public void extendPayload(String additionalPayload) {
		payload.append("\n");
		payload.append(additionalPayload);
	}
}
