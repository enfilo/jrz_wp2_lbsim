package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.statistics.MsgStatistics;
import at.enfilo.lbsim.model.task.*;
import org.simgrid.msg.*;

import java.util.HashMap;

public abstract class StateInfoProvider extends BaseProcess {
	private static final HashMap<String, StateInfoProvider> instances = new HashMap<>();

	protected Worker worker;

	/**
	 * Constructs a new process from a host and his name, the arguments of here method function are
	 * specified by the parameter args.
	 *
	 * @param host The host of the process to create.
	 * @param name The name of the process.
	 * @param args The arguments of main method of the process.
	 */
	public StateInfoProvider(Host host, String name, String[] args) {
		super(host, name, args);
		instances.put(getHost().getName(), this);
	}


	public static StateInfoProvider getInstance(String hostName) {
		return instances.get(hostName);
	}


	/**
	 * Setup process.
	 *
	 * @param args Arguments from deployment.xml
	 * @return Name of own Mailbox to listen. If null is returned no listen.
	 */
	@Override
	protected String setup(String[] args) {
		setupArguments(args);
		return ProcessHelper.constructMailboxName(this.getClass(), getHost().getName());
	}


	/**
	 * Setup and parse arguments.
	 * @param args	Arguments from deployment.xml.
	 */
	protected abstract void setupArguments(String[] args);


	/**
	 * Parse an incoming task / message.
	 *
	 * @param type Type of task.
	 * @param task Task.
	 */
	@Override
	protected void parseAbstractTask(TaskType type, AbstractTask task) {
		// Statistics
		MsgStatistics.receivedMessage(getHost().getName(), this.getClass().getName(), type.name(), task.getMessageSize());

		switch (type) {
			case ORDER:
				Order order = Order.class.cast(task);
				switch (order.getCmd()) {
					case REQUEST_STATE:
						sendStateInfo(order.getSource().getName(), order.getSender().getClass());
						break;

					case LIST_TASKS:
						sendTasksInfo(order.getPayload());
						break;

					default:
						throw new RuntimeException("Command not support: " + order.getCmd());
				}
				break;

			default:
				throw new RuntimeException("Unknown task received: " + task);
		}
	}


	/**
	 * Sends complete state information to an {@link at.enfilo.lbsim.model.process.InitialScheduler} instance or
	 * a {@link at.enfilo.lbsim.model.process.LoadBalancer} instance.
	 *
	 * @param receiverHost	Receiver hostname.
	 * @param receiverProcess	Receiver process.
	 */
	protected void sendStateInfo(String receiverHost, Class receiverProcess) {
		StateInfo si = new StateInfo();
		si.setMetricValue(Metric.CPU_SPEED, getHost().getSpeed());
		si.setMetricValue(Metric.CPU_CORES, getHost().getCoreNumber());
		// Not supported
		//si.setMetricValue(Metric.LOAD, getHost().getLoad());
		si.setMetricValue(Metric.QUEUE_LENGTH_ALL_JOBS, worker.listTasks().getNumberOfTasks());
		si.setMetricValue(Metric.QUEUE_LENGTH, worker.listTasks().getNumberOfTasks(Job.getHighestActivePrio()));

		// Statistics
		MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), si.getType().name(), si.getMessageSize());

		try {
			si.send(ProcessHelper.constructMailboxName(receiverProcess, receiverHost));
		} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
			e.printStackTrace();
		}
	}


	protected void sendStateInfoAsync(String receiverHost, Class receiverProcess) {
		StateInfo si = new StateInfo();
		si.setMetricValue(Metric.CPU_SPEED, getHost().getSpeed());
		si.setMetricValue(Metric.CPU_CORES, getHost().getCoreNumber());
		// Not supported
		//si.setMetricValue(Metric.LOAD, getHost().getLoad());
		si.setMetricValue(Metric.QUEUE_LENGTH_ALL_JOBS, worker.listTasks().getNumberOfTasks());
		si.setMetricValue(Metric.QUEUE_LENGTH, worker.listTasks().getNumberOfTasks(Job.getHighestActivePrio()));

		// Statistics
		MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), si.getType().name(), si.getMessageSize());

		si.dsend(ProcessHelper.constructMailboxName(receiverProcess, receiverHost));
	}

	/**
	 * Send task(s) information to given mailbox.
	 *
	 * @param mailbox	Recipient.
	 */
	protected void sendTasksInfo(String mailbox) {
		TasksInfo ti = worker.listTasks();

		// Statistics
		MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), ti.getType().name(), ti.getMessageSize());

		try {
			ti.send(mailbox);
		} catch (TransferFailureException | HostFailureException | TimeoutException | NativeException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Get assigned worker instance.
	 * @return	{@link at.enfilo.lbsim.model.process.Worker} instance.
	 */
	public Worker getWorker() {
		return worker;
	}


	/**
	 * Set worker instance.
	 * @param worker	{@link at.enfilo.lbsim.model.process.Worker} instance.
	 */
	public void setWorker(Worker worker) {
		this.worker = worker;
	}


	/**
	 * Update from {@link at.enfilo.lbsim.model.process.Worker} about the actual tasks.
	 *
	 * @param ti		All tasks at local {@link at.enfilo.lbsim.model.process.Worker} process.
	 * @param event	{@link at.enfilo.lbsim.model.process.SourceEvent} which initiate this update.
	 */
	public abstract void update(TasksInfo ti, SourceEvent event);
}
