package at.enfilo.lbsim.model.statistics;

import org.simgrid.msg.Msg;

class WorkerDataItem {
	private final double timestamp;
	private final int queueId;
	private final int lenght;

	WorkerDataItem(int queueId, int lenght) {
		this.timestamp = Msg.getClock();
		this.queueId = queueId;
		this.lenght = lenght;
	}

	double getTimestamp() {
		return timestamp;
	}

	int getQueueId() {
		return queueId;
	}

	int getLenght() {
		return lenght;
	}
}
