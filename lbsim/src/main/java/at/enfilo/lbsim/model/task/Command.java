package at.enfilo.lbsim.model.task;

public enum Command {
	CREATE_JOB,
	CREATE_TASK,
	SUBMIT_JOB,
	REQUEST_STATE,
	LIST_TASKS,
	MOVE_TASKS,
	SHUTDOWN,
	FETCH_TASK,
	SEND_TASK,
	REQUEST_DATA
}
