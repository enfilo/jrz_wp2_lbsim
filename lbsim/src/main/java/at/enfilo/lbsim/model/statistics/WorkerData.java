package at.enfilo.lbsim.model.statistics;

import java.util.LinkedList;
import java.util.List;

class WorkerData {
	private final String hostname;
	private final List<WorkerDataItem> history;
	private double speed;
	private int cores;
	private int taskProcessed;
	private double flopsProcessed;

	WorkerData(String hostname, double speed, int cores) {
		this.hostname = hostname;
		this.speed = speed;
		this.cores = cores;
		history = new LinkedList<>();
	}

	WorkerData(String hostname) {
		this(hostname, -1, -1);
	}

	void addWorkerDataItem(WorkerDataItem item) {
		history.add(item);
	}

	String getHostname() {
		return hostname;
	}

	List<WorkerDataItem> getHistory() {
		return history;
	}

	void processedTask(double flopsProcessed) {
		taskProcessed++;
		this.flopsProcessed += flopsProcessed;
	}

	int getTaskProcessed() {
		return taskProcessed;
	}

	double getSpeed() {
		return speed;
	}

	int getCores() {
		return cores;
	}

	double getFlopsProcessed() {
		return flopsProcessed;
	}
}
