package at.enfilo.lbsim.model.task;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class TasksInfo extends AbstractTask {
	private static final String NAME_PREFIX = "TasksInfo_";
	private static long idCounter = 0;

	private final List<Task> tasks;
	private final HashMap<Integer, Integer> tasksPerPrio;

	public TasksInfo(List<Task> tasks) {
		// Size: taskId and taskPrio and taskSize --> 2*int * #tasks + 1*double * #tasks
		super(TaskType.TASKS_INFO, NAME_PREFIX + (idCounter++), 0, ((AbstractTask.INT_SIZE * 2 + AbstractTask.DOUBLE_SIZE) * tasks.size()));
		this.tasks = tasks;
		this.tasksPerPrio = new HashMap<>();
		setupPrios();
	}

	private void setupPrios() {
		for (Task t : tasks) {
			int prio = t.getJobId();
			if (!tasksPerPrio.containsKey(prio)) {
				tasksPerPrio.put(prio, 0);
			}
			tasksPerPrio.put(prio, tasksPerPrio.get(prio) + 1);
		}
	}

	public int getNumberOfTasks() {
		return tasks.size();
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public int getNumberOfTasks(int prio) {
		if (tasksPerPrio.containsKey(prio)) {
			return tasksPerPrio.get(prio);
		}
		return 0;
	}

	public List<Task> getTasks(int prio) {
		return tasks
				.stream()
				.filter(task -> task.getJobId() == prio)
				.collect(Collectors.toList());
	}
}
