package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.statistics.JobStatistics;
import at.enfilo.lbsim.model.statistics.WorkerStatistics;
import at.enfilo.lbsim.model.task.*;
import at.enfilo.lbsim.model.task.Task;
import java.util.UUID;
import org.simgrid.msg.*;

public class WorkerThread extends BaseProcess {
	private static int idCounter = 0;
	private final int id;
	private Worker worker;
	private int suspendCounter;
	private boolean idle = false;
	private final Object idleLock = new Object();

	/**
	 * Constructs a new WorkerThread process from a host and his name, the arguments of here method function are
	 * specified by the parameter args.
	 *
	 * @param worker Worker instance
	 * @param host The host of the process to create.
	 * @param name The name of the process.
	 * @param args The arguments of main method of the process.
	 */
	public WorkerThread(Worker worker, Host host, String name, String[] args) {
		this(host, name, args);
		this.worker = worker;
	}


	/**
	 * Constructs a new process from a host and his name, the arguments of here method function are
	 * specified by the parameter args.
	 *
	 * @param host The host of the process to create.
	 * @param name The name of the process.
	 * @param args The arguments of main method of the process.
	 */
	public WorkerThread(Host host, String name, String[] args) {
		super(host, name, args);
		this.id = idCounter++;
		Msg.verb("New WorkerThread launched: " + id);
	}


	/**
	 * Setup process.
	 *
	 * @param args Arguments from deployment.xml
	 * @return Name of own Mailbox to listen. If null is returned no listen.
	 */
	@Override
	protected String setup(String[] args) {
		setIdle();
		return ProcessHelper.constructMailboxName(WorkerThread.class, getHost().getName());
	}


	/**
	 * Parse an incoming task / message.
	 *
	 * @param type Type of task.
	 * @param task Task.
	 */
	@Override
	protected void parseAbstractTask(TaskType type, AbstractTask task) {
		setBusy();
		switch (type) {
			case ORDER:
				Order order = Order.class.cast(task);
				switch (order.getCmd()) {
					case FETCH_TASK:
						Task t = worker.nextTask();
						if (t != null) {
							executeTask(t);
							setIdle();
							Order sendTask = new Order(Command.SEND_TASK);
							sendTask.dsend(ProcessHelper.constructMailboxName(Worker.class, getHost().getName()));
						}
						break;

					default:
						throw new RuntimeException("Command not supported: " + order.getCmd());
				}
				break;

			default:
				throw new RuntimeException("Unknown task received: " + task);
		}
	}


	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {
		worker.deregister(this);
		worker = null;
	}


	/**
	 * Executes/runs a task on this worker.
	 * Before running all data will be fetched from the data providers.
	 *
	 * @param t 	Task to execute/run.
	 */
	private void executeTask(Task t) {
		Msg.verb("WorkerThread " + id + " exec task " + t.getTaskId());

		// Fetch all needed data before executing task
		JobStatistics.startFetchTaskData(t.getJobId(), t.getTaskId());
		try {
			if (t.getRequiredData() > 0) {
				// Format of payload: '<mailbox>;<amountOfBytes>'
				String dataMailbox = UUID.randomUUID().toString();
				Order request = new Order(Command.REQUEST_DATA, dataMailbox + ";" + t.getRequiredData());
				request.send(ProcessHelper.constructMailboxName(DataProvider.class, worker.getNearestDataProvider()));
				Task.receive(dataMailbox);
			}
		} catch (TransferFailureException | TimeoutException | NativeException | HostFailureException e) {
			e.printStackTrace();
		}
		JobStatistics.endFetchTaskData(t.getJobId(), t.getTaskId());

		// Execute task: 1. recursion, 2. sub-tasks, 3. single task
		JobStatistics.startExecTask(t.getJobId(), t.getTaskId());
		// First: check of recursion
		if (t.getRecursions() > 0) {
			// Create new Task for recursion
			Task recursionTask = t.cloneForRecursion(this);
			JobStatistics.registerTask(recursionTask.getJobId(), recursionTask.getTaskId(), t.getRootOfRecursion());
			// Launch a new worker thread while this is thread suspends
			suspendCounter++;
			worker.launchWorkerThread();
			worker.queueTask(recursionTask, true);
			this.suspend();
		}
		// Second: check of Sub-tasks
		if (t.hasSubTasks()) {
			worker.launchWorkerThread();
			for (Task subTask : t.getSubTasks()) {
				suspendCounter++;
				subTask.setOriginWorkerThread(this);
				JobStatistics.registerTask(subTask.getJobId(), subTask.getTaskId(), t.getTaskId());
				worker.queueTask(subTask, true);
			}
			this.suspend();
		}
		// Third: execute task
		try {
			t.execute();
		} catch (HostFailureException | TaskCancelledException e) {
			e.printStackTrace();
		}
		JobStatistics.endExecTask(t.getJobId(), t.getTaskId());

		// Store result
		JobStatistics.startStoreTaskResult(t.getJobId(), t.getTaskId());
		Data resultData = new Data(t.getResultData());
		try {
			resultData.send(ProcessHelper.constructMailboxName(DataProvider.class, worker.getNearestDataProvider()));
		} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
			e.printStackTrace();
		}
		JobStatistics.endStoreTaskResult(t.getJobId(), t.getTaskId());

		// Finished executing task
		WorkerStatistics.processedTask(getHost().getName(), t.getComputeDuration());
		JobStatistics.finishedTask(t.getJobId(), t.getTaskId());
		Msg.verb("WorkerThread " + id + " exec task " + t.getTaskId() + " done");
		t.finished();

		// Callback to origin worker if needed
		WorkerThread origin = t.getOriginWorkerThread();
		if (origin != null) {
			origin.suspendCounter--;
			if (origin.suspendCounter <= 0) {
				// Close a WorkerThread and resume origin
				origin.resume();
				origin.worker.shutdownWorkerThread();
			}
		}
	}


	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() { }


	void setBusy() {
		synchronized (idleLock) {
			idle = false;
		}
	}

	private void setIdle() {
		synchronized (idleLock) {
			idle = true;
		}
	}

	boolean isIdle() {
		synchronized (idleLock) {
			return idle;
		}
	}
}
