package at.enfilo.lbsim.model.process;

import org.simgrid.msg.Host;
import org.simgrid.msg.Msg;
import org.simgrid.msg.MsgException;
import org.simgrid.msg.Process;

import java.util.LinkedList;
import java.util.List;


class PeriodicProcess extends Process {

	private static boolean active = true;
	private static final List<PeriodicProcess> periodicProcessList = new LinkedList<>();

	private boolean running = false;
	private final BaseProcess processToActivate;
	private final double periodicInterval;

	/**
	 * Constructs a new process from a host and his name, the arguments of here method function are
	 * specified by the parameter args.
	 *
	 * @param host The host of the process to create.
	 * @param name The name of the process.
	 */
	public PeriodicProcess(BaseProcess process, double interval, Host host, String name) {
		super(host, name);
		processToActivate = process;
		periodicInterval = interval;
		periodicProcessList.add(this);
	}

	/**
	 * The main function of the process (to implement).
	 *
	 * @param args
	 * @throws MsgException
	 */
	@Override
	public void main(String[] args) throws MsgException {
		running = true;
		while (active) {
			waitFor(periodicInterval);
			if (active) {
				processToActivate.periodicActivator();
			}
		}
		running = false;
		Msg.verb("Periodic process down...");
	}

	static void shutdown() {
		active = false;
	}

	static int countRunningPeriodicProcesses() {
		int count = 0;
		for (PeriodicProcess pp : periodicProcessList) {
			if (pp.running) {
				count++;
			}
		}
		return count;
	}
}
