package at.enfilo.lbsim.model.util;

import org.apache.commons.math3.distribution.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class ScenarioGenerator {
	private static AbstractRealDistribution subDistFlops;
	private static AbstractRealDistribution subDistTaskSize;
	private static AbstractRealDistribution subDistRequestedSize;
	private static AbstractRealDistribution subDistResultSize;
	private static AbstractIntegerDistribution subDistRecursions;

	private static void generateScenario(
			String scenarioFile,
			AbstractIntegerDistribution distNrJobs,
			AbstractIntegerDistribution distNrTasks,
			AbstractRealDistribution distFlops,
			AbstractRealDistribution distTaskSize,
			AbstractRealDistribution distRequestedSize,
			AbstractRealDistribution distResultSize,
			AbstractRealDistribution distDelay,
			AbstractIntegerDistribution distRecursions,
			AbstractIntegerDistribution distNrSubTasks
	) {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(scenarioFile))) {

			int jobs = distNrJobs.sample();
			// iterate through jobs
			for (int j = 0; j < jobs; j++) {
				// format: j;<delay>
				writer.write("j;" + distDelay.sample());
				writer.newLine();

				int tasks = distNrTasks.sample();
				// iterate through random tasks
				for (int t = 0; t < tasks; t++) {
					double delay = distDelay.sample();
					double flops = distFlops.sample();
					double taskSize = distTaskSize.sample();
					double requestedSize = distRequestedSize.sample();
					double resultSize = distResultSize.sample();
					int recursions = distRecursions.sample();
					// format: t;<delay>;<flops>;<taskSize>;<requestedSize>;<resultSize>;<recursions>
					writer.write(String.format("t;%.2f;%.2f;%.2f;%.2f;%.2f;%d", delay, flops, taskSize, requestedSize, resultSize, recursions));
					writer.newLine();
					int subTasks = distNrSubTasks.sample();
					for (int s = 0; s < subTasks; s++) {
						writer.write(generateSubTask());
						writer.newLine();
					}
				}
			}
			writer.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static String generateSubTask() {
		double flops = subDistFlops.sample();
		double taskSize = subDistTaskSize.sample();
		double requestedSize = subDistRequestedSize.sample();
		double resultSize = subDistResultSize.sample();
		int recursions = subDistRecursions.sample();
		return String.format("s;0.0;%.2f;%.2f;%.2f;%.2f;%d", flops, taskSize, requestedSize, resultSize, recursions);
	}


	public static void generateSimpleUniform(
							String scenarioFile,
							int minJobs, int maxJobs,
							int minTask, int maxTasks,
							double minFlops, double maxFlops,
							double minTaskSize, double maxTaskSize,
							double minRequestedSize, double maxRequestedSize,
							double minResultSize, double maxResultSize,
							double maxDelay
					) {

		AbstractIntegerDistribution distJobs = new UniformIntegerDistribution(minJobs, maxJobs);
		AbstractIntegerDistribution distTasks = new UniformIntegerDistribution(minTask, maxTasks);

		AbstractRealDistribution distFlops;
		if (minFlops == maxFlops) {
			distFlops = new ConstantRealDistribution(maxFlops);
		} else {
			distFlops = new UniformRealDistribution(minFlops, maxFlops);
		}

		AbstractRealDistribution distTaskSize;
		if (minTaskSize == maxTaskSize) {
			distTaskSize = new ConstantRealDistribution(maxTaskSize);
		} else {
			distTaskSize = new UniformRealDistribution(minTaskSize, maxTaskSize);
		}

		AbstractRealDistribution distRequestedSize;
		if (minRequestedSize == maxRequestedSize) {
			distRequestedSize = new ConstantRealDistribution(maxRequestedSize);
		} else {
			distRequestedSize = new UniformRealDistribution(minRequestedSize, maxRequestedSize);
		}

		AbstractRealDistribution distResultSize;
		if (minResultSize == maxResultSize) {
			distResultSize = new ConstantRealDistribution(maxResultSize);
		} else {
			distResultSize = new UniformRealDistribution(minResultSize, maxResultSize);
		}

		AbstractRealDistribution distDelay;
		if (maxDelay == 0) {
			distDelay = new ConstantRealDistribution(0);
		} else {
			distDelay = new UniformRealDistribution(0, maxDelay);
		}
		AbstractIntegerDistribution distRecursions = new UniformIntegerDistribution(0, 0);
		AbstractIntegerDistribution distSubTasks = new UniformIntegerDistribution(0, 0);
		generateScenario(scenarioFile, distJobs, distTasks, distFlops, distTaskSize, distRequestedSize, distResultSize, distDelay, distRecursions, distSubTasks);
	}


	public static void generateSimpleNormalDist(
							String scenarioFile,
							int minJobs, int maxJobs,
							int minTask, int maxTasks,
							double meanFlops, double sdFlops,
							double meanTaskSize, double sdTaskSize,
							double meanRequestedSize, double sdRequestedSize,
							double meanResultSize, double sdResultSize,
							double meanDelay
						) {
		AbstractIntegerDistribution distJobs = new UniformIntegerDistribution(minJobs, maxJobs);
		AbstractIntegerDistribution distTasks = new UniformIntegerDistribution(minTask, maxTasks);

		AbstractRealDistribution distFlops;
		if (sdFlops == 0) {
			distFlops = new ConstantRealDistribution(meanFlops);
		} else {
			distFlops = new NormalDistribution(meanFlops, sdFlops);
		}

		AbstractRealDistribution distTaskSize;
		if (sdTaskSize == 0) {
			distTaskSize = new ConstantRealDistribution(meanTaskSize);
		} else {
			distTaskSize = new NormalDistribution(meanTaskSize, sdTaskSize);
		}

		AbstractRealDistribution distRequestedSize;
		if (sdRequestedSize == 0) {
			distRequestedSize = new ConstantRealDistribution(meanRequestedSize);
		} else {
			distRequestedSize = new NormalDistribution(meanRequestedSize, sdRequestedSize);
		}

		AbstractRealDistribution distResultSize;
		if (sdResultSize == 0) {
			distResultSize = new ConstantRealDistribution(meanResultSize);
		} else {
			distResultSize = new NormalDistribution(meanResultSize, sdResultSize);
		}

		AbstractRealDistribution distDelay;
		if (meanDelay == 0) {
			distDelay = new ConstantRealDistribution(meanDelay);
		} else {
			distDelay = new ExponentialDistribution(meanDelay);
		}
		AbstractIntegerDistribution distRecursions = new UniformIntegerDistribution(0, 0);
		AbstractIntegerDistribution distSubTasks = new UniformIntegerDistribution(0, 0);
		generateScenario(scenarioFile, distJobs, distTasks, distFlops, distTaskSize, distRequestedSize, distResultSize, distDelay, distRecursions, distSubTasks);
	}


	public static void generateRecursionNormalDist(
			String scenarioFile,
			int minJobs, int maxJobs,
			int minTask, int maxTasks,
			double meanFlops, double sdFlops,
			double meanTaskSize, double sdTaskSize,
			double meanRequestedSize, double sdRequestedSize,
			double meanResultSize, double sdResultSize,
			int minRecursions, int maxRecursions,
			double meanDelay
	) {
		AbstractIntegerDistribution distJobs = new UniformIntegerDistribution(minJobs, maxJobs);
		AbstractIntegerDistribution distTasks = new UniformIntegerDistribution(minTask, maxTasks);

		AbstractRealDistribution distFlops;
		if (sdFlops == 0) {
			distFlops = new ConstantRealDistribution(meanFlops);
		} else {
			distFlops = new NormalDistribution(meanFlops, sdFlops);
		}

		AbstractRealDistribution distTaskSize;
		if (sdTaskSize == 0) {
			distTaskSize = new ConstantRealDistribution(meanTaskSize);
		} else {
			distTaskSize = new NormalDistribution(meanTaskSize, sdTaskSize);
		}

		AbstractRealDistribution distRequestedSize;
		if (sdRequestedSize == 0) {
			distRequestedSize = new ConstantRealDistribution(meanRequestedSize);
		} else {
			distRequestedSize = new NormalDistribution(meanRequestedSize, sdRequestedSize);
		}

		AbstractRealDistribution distResultSize;
		if (sdResultSize == 0) {
			distResultSize = new ConstantRealDistribution(meanResultSize);
		} else {
			distResultSize = new NormalDistribution(meanResultSize, sdResultSize);
		}

		AbstractRealDistribution distDelay;
		if (meanDelay == 0) {
			distDelay = new ConstantRealDistribution(meanDelay);
		} else {
			distDelay = new ExponentialDistribution(meanDelay);
		}
		AbstractIntegerDistribution distSubTasks = new UniformIntegerDistribution(0, 0);
		AbstractIntegerDistribution distRecursion = new UniformIntegerDistribution(minRecursions, maxRecursions);
		generateScenario(scenarioFile, distJobs, distTasks, distFlops, distTaskSize, distRequestedSize, distResultSize, distDelay, distRecursion, distSubTasks);
	}


	public static void generateSubtaskNormalDist(
			String scenarioFile,
			int minJobs, int maxJobs,
			int minTask, int maxTasks,
			double meanFlops, double sdFlops,
			double meanTaskSize, double sdTaskSize,
			double meanRequestedSize, double sdRequestedSize,
			double meanResultSize, double sdResultSize,
			int minSubtasks, int maxSubtasks,
			double meanDelay
	) {
		AbstractIntegerDistribution distJobs = new UniformIntegerDistribution(minJobs, maxJobs);
		AbstractIntegerDistribution distTasks = new UniformIntegerDistribution(minTask, maxTasks);

		AbstractRealDistribution distFlops;
		if (sdFlops == 0) {
			distFlops = new ConstantRealDistribution(meanFlops);
			subDistFlops = new ConstantRealDistribution(meanFlops);
		} else {
			distFlops = new NormalDistribution(meanFlops, sdFlops);
			subDistFlops = new NormalDistribution(meanFlops, sdFlops);
		}

		AbstractRealDistribution distTaskSize;
		if (sdTaskSize == 0) {
			distTaskSize = new ConstantRealDistribution(meanTaskSize);
			subDistTaskSize = new ConstantRealDistribution(meanTaskSize);
		} else {
			distTaskSize = new NormalDistribution(meanTaskSize, sdTaskSize);
			subDistTaskSize = new NormalDistribution(meanTaskSize, sdTaskSize);
		}

		AbstractRealDistribution distRequestedSize;
		if (sdRequestedSize == 0) {
			distRequestedSize = new ConstantRealDistribution(meanRequestedSize);
			subDistRequestedSize = new ConstantRealDistribution(meanRequestedSize);
		} else {
			distRequestedSize = new NormalDistribution(meanRequestedSize, sdRequestedSize);
			subDistRequestedSize = new NormalDistribution(meanRequestedSize, sdRequestedSize);
		}

		AbstractRealDistribution distResultSize;
		if (sdResultSize == 0) {
			distResultSize = new ConstantRealDistribution(meanResultSize);
			subDistResultSize = new ConstantRealDistribution(meanResultSize);
		} else {
			distResultSize = new NormalDistribution(meanResultSize, sdResultSize);
			subDistResultSize = new NormalDistribution(meanResultSize, sdResultSize);
		}

		AbstractRealDistribution distDelay;
		if (meanDelay == 0) {
			distDelay = new ConstantRealDistribution(meanDelay);
		} else {
			distDelay = new ExponentialDistribution(meanDelay);
		}
		AbstractIntegerDistribution distRecursion  = new UniformIntegerDistribution(0, 0);
		subDistRecursions = distRecursion;
		AbstractIntegerDistribution distSubTasks= new UniformIntegerDistribution(minSubtasks, maxSubtasks);
		generateScenario(scenarioFile, distJobs, distTasks, distFlops, distTaskSize, distRequestedSize, distResultSize, distDelay, distRecursion, distSubTasks);
	}


	public static void main(String[] args) {

//		generateSimpleUniform("simple_uniform_scenario.txt",
//				1, 2,
//				10000, 10001,
//				1e11, 1e12,
//				1e5, 2e5,
//				1e7, 1e7,
//				1e5, 2e5,
//				0);

//		generateSimpleNormalDist("simple_normaldist_scenario.txt",
//				1, 1,
//				15000, 15000,
//				1e11, 1e9,
//				1e6, 1e4,
//				50e6, 0,
//				10e6, 1e4,
//				0
//				);

//		generateRecursionNormalDist("recursion_normaldist_scenario.txt",
//				1, 1,
//				200, 200,
//				1e11, 1e9,
//				1e5, 1e4,
//				0, 0,
//				1e5, 1e4,
//				0, 8,
//				0
//				);


		generateSubtaskNormalDist("subtasks_nromaldist_scenario.txt",
				1, 1,
				5000, 5000,
				1e10, 1e9,
				1e6, 1e4,
				100e6, 0,
				10e6, 1e4,
				0, 10,
				0
				);
	}
}
