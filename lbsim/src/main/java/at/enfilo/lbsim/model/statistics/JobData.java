package at.enfilo.lbsim.model.statistics;

import org.simgrid.msg.Msg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Job statistic data
 */
class JobData {
    private final int id;
    private final HashMap<Integer, TaskData> taskMap;
    private final List<TaskData> finishedTasks;
    private final double creationTime;
    private double startTime;
    private double endTime;
	private double completeExecTime;
	private double completeQueueTime;
	private double completeFetchDataTime;
	private double completeStoreResultTime;
	private double completeScheduleTime;

    JobData(int id) {
        this.id = id;
        taskMap = new HashMap<>();
        finishedTasks = new ArrayList<>();
        creationTime = Msg.getClock();
    }

    void registerTask(TaskData td) {
        taskMap.put(td.getTaskId(), td);
    }


	int getId() {
		return id;
	}


	double getCreationTime() {
		return creationTime;
	}


	double getEndTime() {
		return endTime;
	}


	double getStartTime() {
		return startTime;
	}


	boolean isFinished() {
		return endTime > 0;
	}


	List<TaskData> getFinishedTasks() {
		return finishedTasks;
	}


	void moveTask(int taskId) {
		if (taskMap.containsKey(taskId)) {
			taskMap.get(taskId).moveTask();
		}
	}


	void startFetchTaskData(int taskId) {
		if (taskMap.containsKey(taskId)) {
			taskMap.get(taskId).startFetchData();
			if (startTime == 0) {
				startTime = taskMap.get(taskId).getStartFetchDataTime();
			}
		}
	}


	void endFetchTaskData(int taskId) {
		if (taskMap.containsKey(taskId)) {
			TaskData td = taskMap.get(taskId);
			td.endFetchData();
			completeFetchDataTime += (td.getEndFetchDataTime() - td.getStartFetchDataTime());
		}
	}


	void startExecTask(int taskId) {
		if (taskMap.containsKey(taskId)) {
			taskMap.get(taskId).startExec();
		}
	}


	void endExecTask(int taskId) {
		if (taskMap.containsKey(taskId)) {
			TaskData td = taskMap.get(taskId);
			td.endExec();
			completeExecTime += (td.getEndExecTime() - td.getStartExecTime());
		}
	}


	void startStoreTaskResult(int taskId) {
		if (taskMap.containsKey(taskId)) {
			taskMap.get(taskId).startStoreResult();
		}
	}


	void endStoreTaskResult(int taskId) {
		if (taskMap.containsKey(taskId)) {
			TaskData td = taskMap.get(taskId);
			td.endStoreResult();
			completeStoreResultTime += (td.getEndStoreResultTime() - td.getStartStoreResultTime());
		}
	}


	void finishedTask(int taskId) {
		if (taskMap.containsKey(taskId)) {
			TaskData td = taskMap.get(taskId);
			td.finished();
			finishedTasks.add(td);
			taskMap.remove(taskId);
//			if (taskMap.isEmpty()) {
//				endTime = td.getEndExecTime();
//			}
		}
	}

	int getNumberOfTasks() {
		return finishedTasks.size() + taskMap.size();
	}


	void startQueueTask(int taskId) {
		if (taskMap.containsKey(taskId)) {
			TaskData td = taskMap.get(taskId);
			td.startQueue();
			completeScheduleTime += (td.getStartQueueTime() - td.getCreationTime());
		}
	}

	void endQueueTask(int taskId) {
		if (taskMap.containsKey(taskId)) {
			TaskData td = taskMap.get(taskId);
			td.endQueue();
			completeQueueTime += (td.getEndQueueTime() - td.getStartQueueTime());
		}
	}

	void finishedJob() {
		endTime = Msg.getClock();
	}

	double getCompleteExecTime() {
		return completeExecTime;
	}

	double getCompleteQueueTime() {
		return completeQueueTime;
	}

	double getCompleteFetchDataTime() {
		return completeFetchDataTime;
	}

	double getCompleteStoreResultTime() {
		return completeStoreResultTime;
	}

	double getCompleteScheduleTime() {
		return completeScheduleTime;
	}

}
