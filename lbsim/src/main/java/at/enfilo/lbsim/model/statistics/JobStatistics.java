package at.enfilo.lbsim.model.statistics;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class JobStatistics {
	public static final String DEFAULT_JOB_FILE_NAME = "job_stats.csv";
	public static final String DEFAULT_TASK_FILE_NAME = "task_stats.csv";

	private static final HashMap<Integer, JobData> jobMap = new HashMap<>();


    public static void registerJob(int jobId) {
        JobData jd = new JobData(jobId);
        jobMap.put(jobId, jd);
    }


    public static void registerTask(int jobId, int taskId, int order) {
        TaskData td = new TaskData(taskId, order);
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).registerTask(td);
		}
    }


	public static void startFetchTaskData(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).startFetchTaskData(taskId);
		}
	}


	public static void endFetchTaskData(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).endFetchTaskData(taskId);
		}
	}


    public static void startExecTask(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).startExecTask(taskId);
		}
    }


    public static void endExecTask(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).endExecTask(taskId);
		}
    }


	public static void startStoreTaskResult(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).startStoreTaskResult(taskId);
		}
	}


	public static void endStoreTaskResult(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).endStoreTaskResult(taskId);
		}
	}


	public static void moveTask(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).moveTask(taskId);
		}
	}


	public static void finishedTask(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).finishedTask(taskId);
		}
	}


	public static void startQueueTask(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).startQueueTask(taskId);
		}
	}


	public static void endQueueTask(int jobId, int taskId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).endQueueTask(taskId);
		}
	}


	public static void finishedJob(int jobId) {
		if (jobMap.containsKey(jobId)) {
			jobMap.get(jobId).finishedJob();
		}
	}

	public static void writeStatistics(String jobReportFile, String taskReportFile) {
		// Write job statistics
		try (CSVWriter writer = new CSVWriter(new FileWriter(jobReportFile))) {
			String[] title = new String[]{
					"JobId", "CreationTime", "StartTime", "EndTime", "Tasks",
					"CompleteScheduleTime", "CompleteQueueTime", "CompleteFetchDataTime", "CompleteExecuteTime", "CompleteStoreResultTime"
			};
			writer.writeNext(title);
			for (JobData jd : jobMap.values()) {
				String[] entries = new String[title.length];
				int i = 0;
				entries[i++] = Integer.toString(jd.getId());
				entries[i++] = Double.toString(jd.getCreationTime());
				entries[i++] = Double.toString(jd.getStartTime());
				entries[i++] = Double.toString(jd.getEndTime());
				entries[i++] = Integer.toString(jd.getNumberOfTasks());
				entries[i++] = Double.toString(jd.getCompleteScheduleTime());
				entries[i++] = Double.toString(jd.getCompleteQueueTime());
				entries[i++] = Double.toString(jd.getCompleteFetchDataTime());
				entries[i++] = Double.toString(jd.getCompleteExecTime());
				entries[i++] = Double.toString(jd.getCompleteStoreResultTime());
				writer.writeNext(entries);
			}
			writer.flush();
			//writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Write task statistics
		try (CSVWriter writer = new CSVWriter(new FileWriter(taskReportFile))) {
			String[] title = new String[]{
					"JobId",
					"TaskId",
					"CreationTime",
					"StartQueueTime",
					"EndQueueTime",
					"StartFetchDataTime",
					"EndFetchDataTime",
					"StartExecTime",
					"EndExecTime",
					"StartStoreResultTime",
					"EndStoreResultTime",
					"FinishedTime",
					"MoveCounter"
			};
			writer.writeNext(title);
			for (JobData jd : jobMap.values()) {
				List<TaskData> tasks = jd.getFinishedTasks();
				tasks.sort(new Comparator<TaskData>() {
					@Override
					public int compare(TaskData td1, TaskData td2) {
						if (td1.getOrder() == td2.getOrder()) {
							return Integer.compare(td1.getTaskId(), td2.getTaskId());
						}
						return Integer.compare(td1.getOrder(), td2.getOrder());
					}
				});

				for (TaskData td : tasks) {
					String[] entries = new String[title.length];
					int i = 0;
					entries[i++] = Integer.toString(jd.getId());
					entries[i++] = Integer.toString(td.getTaskId());
					entries[i++] = Double.toString(td.getCreationTime());
					entries[i++] = Double.toString(td.getStartQueueTime());
					entries[i++] = Double.toString(td.getEndQueueTime());
					entries[i++] = Double.toString(td.getStartFetchDataTime());
					entries[i++] = Double.toString(td.getEndFetchDataTime());
					entries[i++] = Double.toString(td.getStartExecTime());
					entries[i++] = Double.toString(td.getEndExecTime());
					entries[i++] = Double.toString(td.getStartStoreResultTime());
					entries[i++] = Double.toString(td.getEndStoreResultTime());
					entries[i++] = Double.toString(td.getFinishedTime());
					entries[i++] = Integer.toString(td.getMoveCounter());
					writer.writeNext(entries);
				}
			}
			writer.flush();
			//writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void reset() {
		jobMap.clear();
	}


	public static List<JobData> getJobs() {
		return new LinkedList<>(jobMap.values());
	}


	public static void writeStatistics() {
		writeStatistics(DEFAULT_JOB_FILE_NAME, DEFAULT_TASK_FILE_NAME);
	}

}
