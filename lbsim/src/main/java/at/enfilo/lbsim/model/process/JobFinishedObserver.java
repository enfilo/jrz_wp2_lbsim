package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.task.Job;

public interface JobFinishedObserver {

	/**
	 * Callback handler. If job is finished, this method will be called.
	 *
	 * @param job	job which is finished.
	 */
	void notifyFinished(Job job);
}
