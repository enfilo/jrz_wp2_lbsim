package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.task.AbstractTask;
import at.enfilo.lbsim.model.task.Command;
import at.enfilo.lbsim.model.task.Order;
import at.enfilo.lbsim.model.task.TaskType;
import org.simgrid.msg.*;
import org.simgrid.msg.Process;

/**
 * Abstract base class for every process.
 *
 * Provides a listening for new incoming messages/tasks and a shutdown handling.
 * Shutdown handling is based on ORDER-SHUTDOWN message.
 */
public abstract class BaseProcess extends Process {

	private PeriodicProcess pp;

	/**
	 * Constructs a new process from a host and his name, the arguments of here method function are
	 * specified by the parameter args.
	 *
	 * @param host The host of the process to create.
	 * @param name The name of the process.
	 * @param args The arguments of main method of the process.
	 */
	protected BaseProcess(Host host, String name, String[] args) {
		super(host, name, args);
	}

	/**
	 * The main function of the process (to implement).
	 *
	 * @param args
	 * @throws MsgException
	 */
	@Override
	public void main(String[] args) throws MsgException {
		String mailbox = setup(args);
		double interval = periodicInterval();

		// Start periodic process if interval is positive and > 0.0
		if (interval > 0) {
			pp = new PeriodicProcess(this, interval, getHost(), PeriodicProcess.class.getName());
			pp.start();
		}

		// Start listen mailbox if mailbox exists.
		if (mailbox != null) {
			listen(mailbox);
		}
	}


	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return	Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	protected abstract double periodicInterval();


	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	protected abstract void periodicActivator();


	/**
	 * Listen to given Mailbox and parse/delegate message on receipt.
	 *
	 * @param mailbox	Mailbox to listen.
	 */
	private void listen(String mailbox) {
		while (true) {
			Task t = null;
			try {
				// Wait for new message/task
				t = Task.receive(mailbox);

				// Cast to own AbstractTask type
				AbstractTask task = AbstractTask.class.cast(t);

				// Perform a shutdown, if a shutdown order received
				if (task.getType() == TaskType.ORDER) {
					Order order = Order.class.cast(task);
					// Shutdown order
					if (order.getCmd() == Command.SHUTDOWN) {
						Msg.verb("Shutdown received...");
						shutdown();
						break; // End while loop
					}
				}

				// Otherwise parse incoming task
				parseAbstractTask(task.getType(), task);

			} catch (HostFailureException | TransferFailureException | TimeoutException e) {
				Msg.critical(this.getClass().toString());
				Msg.critical(e.getMessage());
				e.printStackTrace();
				break;
			}
		}
	}


	/**
	 * Setup process.
	 *
	 * @param args	Arguments from deployment.xml
	 * @return	Name of own Mailbox to listen. If null is returned no listen.
	 */
	protected abstract String setup(String[] args);


	/**
	 * Parse an incoming task / message.
	 *
	 * @param type	Type of task.
	 * @param task	Task.
	 */
	protected abstract void parseAbstractTask(TaskType type, AbstractTask task);


	/**
	 * Shutdown handler.
	 */
	protected abstract void shutdown();

}
