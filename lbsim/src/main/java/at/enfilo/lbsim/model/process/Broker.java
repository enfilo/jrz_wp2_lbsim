package at.enfilo.lbsim.model.process;

import at.enfilo.lbsim.model.statistics.JobStatistics;
import at.enfilo.lbsim.model.statistics.MsgStatistics;
import at.enfilo.lbsim.model.statistics.WorkerStatistics;
import at.enfilo.lbsim.model.task.*;
import at.enfilo.lbsim.model.task.Task;
import org.simgrid.msg.*;

public class Broker extends BaseProcess implements JobFinishedObserver {

	private String initialSchedulerMailbox;
	private Job currentJob;

	public Broker(Host host, String name, String[] args) {
		super(host, name, args);
	}


	/**
	 * Setup process.
	 *
	 * @param args Arguments from deployment.xml
	 * @return Name of own Mailbox to listen. If null is returned no listen.
	 */
	@Override
	protected String setup(String[] args) {
		if (args.length != 1) {
			throw new RuntimeException("Broker Process need one arguments: initial-scheduler host");
		}

		String initialScheduler = args[0];
		initialSchedulerMailbox = ProcessHelper.constructMailboxName(InitialScheduler.class, initialScheduler);

		return ProcessHelper.constructMailboxName(this.getClass(), getHost().getName());
	}


	/**
	 * Parse an incoming task / message.
	 *
	 * @param type Type of task.
	 * @param task Task.
	 */
	@Override
	protected void parseAbstractTask(TaskType type, AbstractTask task) {
		// Only ORDER and DATA are supported
		try {
			switch (type) {
				case ORDER:
					Order order = Order.class.cast(task);
					switch (order.getCmd()) {
						case CREATE_JOB:
							createJob();
							break;

						case SUBMIT_JOB:
							submitJob(order);
							break;

						case CREATE_TASK:
							createTask(order.getPayload());
							break;

						default:
							throw new RuntimeException("Command not support: " + order.getCmd());
					}
					break;

				default:
					throw new RuntimeException("Unknown task received: " + task);
			}

		} catch (HostFailureException | TransferFailureException | NativeException | TimeoutException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {
		// Add observer to last job, on callback initiate the shutdown
		currentJob.registerObserver(this);
	}


	/**
	 * Creates a new job.
	 */
	private void createJob() {
		currentJob = new Job();
		Msg.verb("Create new job with id " + currentJob.getId());
		// Statistics
		JobStatistics.registerJob(currentJob.getId());
	}


	/**
	 * Creates a new task and delegate it to the InitialScheduler.
	 *
	 * @param taskData	Contains data (payload) to create a task.
	 * 					Payload format: <computationDuration>;<taskSize>;<requiredData>;<resultData>;<recursions>
	 * 					Sub-tasks are possible, if payload has multiple lines.
	 * @throws HostFailureException
	 * @throws NativeException
	 * @throws TimeoutException
	 * @throws TransferFailureException
	 */
	private void createTask(String taskData) throws HostFailureException, NativeException, TimeoutException, TransferFailureException {
		// payload format:
		//   <computationDuration>;<taskSize>;<requiredData>;<resultData>;<recursions>
		Task task = null;
		for (String data : taskData.split("\n")) {
			String[] taskValues = data.split(";");
			double computationDuration = Double.parseDouble(taskValues[0]);
			double taskSize = Double.parseDouble(taskValues[1]);
			double requiredData = Double.parseDouble(taskValues[2]);
			double resultData = Double.parseDouble(taskValues[3]);
			int recursions = Integer.parseInt(taskValues[4]);
			Task t = new Task(currentJob, computationDuration, taskSize, requiredData, resultData, recursions);
			if (task == null) {
				task = t;
			} else {
				task.addSubTask(t);
			}
		}
		if (task != null) {
			Msg.verb("Create new task with id " + task.getTaskId());
			task.send(initialSchedulerMailbox);
			// Statistics
			JobStatistics.registerTask(currentJob.getId(), task.getTaskId(), task.getTaskId());
		}
	}


	/**
	 * Submit a job. Delegates this command to InitialScheduler.
	 *
	 * @param submit 	Order to delegate.
	 * @throws HostFailureException
	 * @throws NativeException
	 * @throws TimeoutException
	 * @throws TransferFailureException
	 */
	private void submitJob(Order submit) throws HostFailureException, NativeException, TimeoutException, TransferFailureException {
		Msg.verb("Submit job " + currentJob.getId());
		submit.send(initialSchedulerMailbox);
	}


	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}


	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() { }


	/**
	 * Callback handler. If job is finished, this method will be called.
	 *
	 * @param job
	 */
	@Override
	public void notifyFinished(Job job) {
		// Export statistics
		JobStatistics.writeStatistics();
		WorkerStatistics.writeStatistics();
		MsgStatistics.writeStatistics();

		// Last job is done, initiate shutdown.
		Order shutdown = new Order(Command.SHUTDOWN);
		try {
			shutdown.send(initialSchedulerMailbox);
		} catch (TransferFailureException | HostFailureException | TimeoutException | NativeException e) {
			e.printStackTrace();
		}
	}
}
