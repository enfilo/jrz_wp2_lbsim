package at.enfilo.lbsim.model;

import org.simgrid.msg.MsgException;

/**
 *
 * @author soma
 */
public class InvalidTaskException extends MsgException {

	public InvalidTaskException(String string) {
		super(string);
	}
	
}
