package at.enfilo.lbsim.algos.global;

import at.enfilo.lbsim.model.process.LoadBalancer;
import at.enfilo.lbsim.model.process.ProcessHelper;
import at.enfilo.lbsim.model.statistics.MsgStatistics;
import at.enfilo.lbsim.model.task.AbstractTask;
import at.enfilo.lbsim.model.task.Metric;
import at.enfilo.lbsim.model.task.StateInfo;
import org.simgrid.msg.Host;
import org.simgrid.msg.HostFailureException;

import java.util.List;

public class LoadInformationCenter extends LoadBalancer {

	private double interval;
	private double delay;
	private LoadVector loadVector;
	private String[] neighbors;

	public LoadInformationCenter(Host host, String name, String[] args) {
		super(host, name, args);
	}

	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *             already parsed and pruned.
	 */
	@Override
	protected void setupFurtherArgs(String[] args) {
		if (args.length != 3) {
			throw new RuntimeException("LoadInformationCenter needs three extra arguments: list of neighbors (workers), interval and delay.\n" +
					"Interval must be same as GlobalStateInfoProvider and delay is the waiting to broadcast load information.");
		}
		neighbors = args[0].split(",");
		interval = Double.parseDouble(args[1]);
		delay = Double.parseDouble(args[2]);
	}

	/**
	 * Initialize scheduler. Will be called after setupFurtherArgs and before start listening.
	 */
	@Override
	protected void init() {
		loadVector = new LoadVector(neighbors.length);
	}


	/**
	 * Received a message / task, type CUSTOM, from another process.
	 * This method is called by receiving to parse the message.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTarget()}
	 * 5. {@link #selectTasks(String)}
	 *
	 * @param custom Message to parse.
	 * @return true: load balancing workflow will be started.
	 * false: Just parse the message.
	 */
	@Override
	protected boolean parseIncomingCustomMessage(AbstractTask custom) {
		return false;
	}

	/**
	 * Estimate workers. Information received with {@link #parseIncomingCustomMessage(AbstractTask)} or
	 * {@link #parseIncomingStateInfo(StateInfo)}.
	 * <p>
	 * This method is the first step of the LoadBalancing Workflow. Next step: {@link #needOfBalance()}.
	 */
	@Override
	protected void estimateWorkers() {

	}

	/**
	 * New StateInformation received. Parse and estimate Worker.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTarget()}
	 * 5. {@link #selectTasks(String)}
	 *
	 * @param stateInfo new StateInfo to parse
	 * @return true: LoadBalancing workflow will be started
	 * false: Just parse the message
	 */
	@Override
	protected boolean parseIncomingStateInfo(StateInfo stateInfo) {
		int load = new Double(stateInfo.getMetricValue(Metric.QUEUE_LENGTH)).intValue();
		loadVector.setLoad(stateInfo.getSource().getName(), load);
		return false;
	}

	/**
	 * Returns how many (re)balance actions are necessary.
	 *
	 * @return	x  > 0: workflow will go ahead and call selectSource(), selectTasks() and selectTarget() x times.
	 *			x <= 0: workflow will be stopped
	 */
	@Override
	protected int needOfBalance() {
		return 0;
	}

	/**
	 * Select a target worker to move the selected task.
	 *
	 * @return hostname of target worker.
	 */
	@Override
	protected String selectTarget() {
		return null;
	}

	/**
	 * Returns a list of task ids to move.
	 *
	 * @param source Source worker hostname.
	 * @return List of Tasks to move.
	 */
	@Override
	protected List<Integer> selectTasks(String source) {
		return null;
	}

	/**
	 * Returns the source (overloaded) worker.
	 *
	 * @return Hostname of worker.
	 */
	@Override
	protected String selectSource() {
		return null;
	}


	/**
	 * Method will be called after advising selected workers to balance load (tasks).
	 * Its the last step of LoadBalancing Workflow to clean up all temporary information.
	 */
	@Override
	protected void cleanupAfterBalance() {

	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return interval;
	}


	/**
	 * Method lbActivator (LoadBalancing Activator) will be called every "interval" seconds.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTarget()}
	 * 5. {@link #selectTasks(String)}
	 * 6. {@link #cleanupAfterBalance()}
	 *
	 * @return true: LoadBalancing workflow will be started
	 * false: Just parse the message
	 */
	@Override
	protected boolean lbActivator() {
		try {
			waitFor(delay);
			if (loadVector.getNrOfLoadInformations() == neighbors.length) {
				for (String worker : neighbors) {
					// Statistics
					MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), loadVector.getType().name(), loadVector.getMessageSize());

					loadVector.clone().dsend(ProcessHelper.constructMailboxName(GlobalLoadBalancer.class, worker));
				}
			}
		} catch (HostFailureException e) {
			e.printStackTrace();
		}
		return false;
	}
}
