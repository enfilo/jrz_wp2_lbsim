package at.enfilo.lbsim.algos.global;

import at.enfilo.lbsim.model.process.SourceEvent;
import at.enfilo.lbsim.model.process.StateInfoProvider;
import at.enfilo.lbsim.model.task.TasksInfo;
import org.simgrid.msg.Host;
import org.simgrid.msg.HostFailureException;

public class GlobalStateInfoProvider extends StateInfoProvider {
	private String LIC; // load information center
	private double interval;
	private int lastSendLoad;
	private int currentLoad;

	public GlobalStateInfoProvider(Host host, String name, String[] args) {
		super(host, name, args);
		lastSendLoad = -1;
		currentLoad = -1;
	}

	/**
	 * Setup and parse arguments.
	 *
	 * @param args Arguments from deployment.xml.
	 */
	@Override
	protected void setupArguments(String[] args) {
		if (args.length != 2) {
			throw new RuntimeException("Two arguments needed: LIC host, interval to send new state updates");
		}

		LIC = args[0];
		interval = Double.parseDouble(args[1]);
	}

	/**
	 * Update from {@link at.enfilo.lbsim.model.process.Worker} about the actual tasks.
	 *
	 * @param ti All tasks at local {@link at.enfilo.lbsim.model.process.Worker} process.
	 */
	@Override
	public void update(TasksInfo ti, SourceEvent event) {
		currentLoad = ti.getNumberOfTasks();
	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return interval;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() {
		if (currentLoad != -1) {
			if (lastSendLoad != currentLoad) {
				sendStateInfo(LIC, LoadInformationCenter.class);
				lastSendLoad = currentLoad;
			}
		}
	}

	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {
	}
}
