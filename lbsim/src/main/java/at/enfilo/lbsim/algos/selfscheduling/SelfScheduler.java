package at.enfilo.lbsim.algos.selfscheduling;

import at.enfilo.lbsim.model.process.InitialScheduler;
import at.enfilo.lbsim.model.process.ProcessHelper;
import at.enfilo.lbsim.model.process.Worker;
import at.enfilo.lbsim.model.task.StateInfo;
import at.enfilo.lbsim.model.task.Task;
import org.simgrid.msg.*;

import java.util.LinkedList;
import java.util.Queue;

public class SelfScheduler extends InitialScheduler {

	private int bundleSize = 1;
	private Queue<Task> sharedQueue;
	private Queue<String> idleWorkerQueue;

	public SelfScheduler(Host host, String name, String[] args) {
		super(host, name, args);
	}

	/**
	 * Initialize scheduler. Will be called after setupFurtherArgs and before start listening.
	 */
	@Override
	protected void init() {
		sharedQueue = new LinkedList<>();
		idleWorkerQueue = new LinkedList<>();
		for (String worker : neighbors) {
			idleWorkerQueue.add(worker);
		}
	}

	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *             already parsed and pruned.
	 */
	@Override
	protected void setupFurtherArgs(String[] args) {
		if (args.length == 1) {
			bundleSize = Integer.parseInt(args[0]);
		}
	}

	/**
	 * This method is only performed if {@link this.waitForSubmit} flag is true.
	 * <p>
	 * Method is called while submitting job and before start scheduling tasks by selecting workers.
	 * After this method {@method selectWorker()} is called for every task in queue.
	 *
	 * @param numberOfTasks Number of tasks to schedule.
	 */
	@Override
	protected void prepareForSubmit(int numberOfTasks) {

	}

	/**
	 * Handle incoming StateInfo task/message.
	 *
	 * @param si StateInfo to handle.
	 */
	@Override
	protected void parseStateInfo(StateInfo si) {
		String worker = si.getSender().getHost().getName();
		idleWorkerQueue.add(worker);
		scheduleNextTasks();
	}

	/**
	 * Select next worker to schedule task to it.
	 *
	 * @return Hostname of worker.
	 */
	@Override
	protected String selectWorker() {
		return null;
	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() {

	}


	/**
	 * Schedule a task to a worker node.
	 *
	 * @param t Task to schedule.
	 */
	@Override
	protected void scheduleTask(Task t) {
		sharedQueue.add(t);
		scheduleNextTasks();
	}

	private void scheduleNextTasks() {
		if (!idleWorkerQueue.isEmpty()) {
			String worker = idleWorkerQueue.poll();
			for (int i = 0; i < bundleSize; i++) {
				if (!sharedQueue.isEmpty()) {
					Task task = sharedQueue.poll();
					String workerMailbox = ProcessHelper.constructMailboxName(Worker.class, worker);
					try {
						task.send(workerMailbox);
					} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
