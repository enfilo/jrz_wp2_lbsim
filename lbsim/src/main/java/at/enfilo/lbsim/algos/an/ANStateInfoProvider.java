package at.enfilo.lbsim.algos.an;

import at.enfilo.lbsim.model.process.SourceEvent;
import at.enfilo.lbsim.model.process.StateInfoProvider;
import at.enfilo.lbsim.model.process.Worker;
import at.enfilo.lbsim.model.task.Job;
import at.enfilo.lbsim.model.task.TasksInfo;
import org.simgrid.msg.Host;

public class ANStateInfoProvider extends StateInfoProvider {
	private String[] masters;
	private int queueLengthLastSend;

	public ANStateInfoProvider(Host host, String name, String[] args) {
		super(host, name, args);
		queueLengthLastSend = -1;
	}

	/**
	 * Setup and parse arguments.
	 *
	 * @param args Arguments from deployment.xml.
	 */
	@Override
	protected void setupArguments(String[] args) {
		if (args.length != 1) {
			throw new RuntimeException("ANStateInfoProvider needs one Argument: hostname of the domain masters, separated by comma");
		}

		masters = args[0].split(",");
	}


	/**
	 * Update from {@link Worker} about the actual tasks.
	 *
	 * @param ti All tasks at local {@link Worker} process.
	 */
	@Override
	public void update(TasksInfo ti, SourceEvent event) {
		int prio = Job.getHighestActivePrio();
		if (queueLengthLastSend != ti.getNumberOfTasks(prio)) {
			switch (event) {
				case FETCH_NEXT_TASK:
					if (ti.getNumberOfTasks(prio) > 0) {
						for (String master : masters) {
							sendStateInfo(master, ANMaster.class);
						}
					}
					break;

				default:
					if (ti.getNumberOfTasks(prio) > 1) {
						for (String master : masters) {
							sendStateInfo(master, ANMaster.class);
						}
					}
			}

		}
	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() {

	}

	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {

	}
}
