package at.enfilo.lbsim.algos.selfscheduling;

import at.enfilo.lbsim.model.process.SourceEvent;
import at.enfilo.lbsim.model.process.StateInfoProvider;
import at.enfilo.lbsim.model.process.Worker;
import at.enfilo.lbsim.model.task.Job;
import at.enfilo.lbsim.model.task.TasksInfo;
import org.simgrid.msg.Host;

/**
 * SelfScheduler StateInfoProvider
 */
public class SSStateInfoProvider extends StateInfoProvider {

	private String master;

	public SSStateInfoProvider(Host host, String name, String[] args) {
		super(host, name, args);
	}

	/**
	 * Setup and parse arguments.
	 *
	 * @param args Arguments from deployment.xml.
	 */
	@Override
	protected void setupArguments(String[] args) {
		if (args.length != 1) {
			throw new RuntimeException("RVStateInfoProvider needs one argument: name of central host.");
		}

		master = args[0];
	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() {

	}

	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {

	}

	/**
	 * Update from {@link Worker} about the actual tasks.
	 *
	 * @param ti    All tasks at local {@link Worker} process.
	 * @param event {@link SourceEvent} which initiate this update.
	 */
	@Override
	public void update(TasksInfo ti, SourceEvent event) {
		if ((event == SourceEvent.FETCH_NEXT_TASK) && (ti.getNumberOfTasks(Job.getHighestActivePrio()) == 0)) {
			sendStateInfo(master, SelfScheduler.class);
		}
	}
}
