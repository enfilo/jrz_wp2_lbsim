package at.enfilo.lbsim.algos.rendezvous;

import at.enfilo.lbsim.model.process.LoadBalancer;
import at.enfilo.lbsim.model.task.*;
import org.simgrid.msg.*;

import java.util.*;

/**
 * Original algorithm from paper "Data-Parallel Load Balancing Strategies", 1998.
 * Paper-Authors: Cyirl Fonlupt, Philippe Marquet and Jean-Luc Dekeyser
 */
public class RVLoadBalancer extends LoadBalancer {
	private final HashMap<String, Integer> workersMap;
	private final List<String> workersList;
	private final HashMap<Integer, StateInfo> stateInfoMap;

	private Integer[] L; // load
	private Integer[] rcv;
	private Integer[] dst;
	private Integer[] valueUnderloaded;
	private Integer[] valueOverloaded;
	private Integer[] friend;
	private Integer[] rendezVous;
	private int threshold;
	private int N;
	private final Queue<Integer> rendezVousQueue;

	private final LinkedList<String> involedLBHosts;


	public RVLoadBalancer(Host host, String name, String[] args) {
		super(host, name, args);
		workersMap = new HashMap<>();
		workersList = new ArrayList<>();
		stateInfoMap = new HashMap<>();
		rendezVousQueue = new LinkedList<>();
		involedLBHosts = new LinkedList<>();
	}

	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *             already parsed and pruned.
	 */
	@Override
	protected void setupFurtherArgs(String[] args) {

	}

	/**
	 * Initialize scheduler. Will be called after setupFurtherArgs and before start listening.
	 */
	@Override
	protected void init() {

	}

	/**
	 * Received a message / task, type CUSTOM, from another process.
	 * This method is called by receiving to parse the message.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTarget()}
	 * 5. {@link #selectTasks(String)}
	 * 6. {@link #cleanupAfterBalance()}
	 *
	 * @param custom Message to parse.
	 * @return true: load balancing workflow will be started.
	 * false: Just parse the message.
	 */
	@Override
	protected boolean parseIncomingCustomMessage(AbstractTask custom) {
		return false;
	}

	/**
	 * Method will be called after advising selected workersMap to balance load (tasks).
	 * Its the last step of LoadBalancing Workflow to clean up all temporary information.
	 */
	@Override
	protected void cleanupAfterBalance() {
		L = null;
		rcv = null;
		dst = null;
		valueOverloaded = null;
		valueUnderloaded = null;
		rendezVous = null;
		friend = null;
		rendezVousQueue.clear();
	}

	/**
	 * Estimate workersMap. Information received with {@link #parseIncomingCustomMessage(AbstractTask)} or
	 * {@link #parseIncomingStateInfo(StateInfo)}.
	 * <p>
	 * This method is the first step of the LoadBalancing Workflow. Next step: {@link #needOfBalance()}.
	 */
	@Override
	protected void estimateWorkers() {
		int prio = Job.getHighestActivePrio();

		// Initialization
		initWorkingArrays();
		int L = 0;
		for (int i = 0; i < N; i++) {
			this.L[i] = new Double(stateInfoMap.get(i).getMetricValue(Metric.QUEUE_LENGTH)).intValue();
			L += this.L[i];
			rcv[i] = null;
			rendezVous[i] = null;
			valueUnderloaded[i] = null;
			valueOverloaded[i] = null;
		}
		threshold = L / N;

		// Underloaded / Overloaded processor sort
		int lowestValue = 0;
		for (int i = 0; i < N; i++) {
			if (this.L[i] < threshold) {
				// Underloaded
				valueUnderloaded[i] = (this.L[i] - threshold);
				if (valueUnderloaded[i] < lowestValue) {
					lowestValue = valueUnderloaded[i];
				}
			} else if (this.L[i] > threshold) {
				// Overloaded
				valueOverloaded[i] = -(this.L[i] - threshold);
				if (valueOverloaded[i] < lowestValue) {
					lowestValue = valueOverloaded[i];
				}
			}
		}
		Integer[] rankUnderloaded = rank(valueUnderloaded, lowestValue);
		Integer[] rankOverloaded = rank(valueOverloaded, lowestValue);

		for (int i = 0; i < N; i++) {
			// Underloaded processor sort
			if (this.L[i] < threshold) {
				dst[i] = rankUnderloaded[i];
				rcv[dst[i]] = i;
			}
			// Overloaded processor sort
			else if (this.L[i] > threshold) {
				friend[i] = rankOverloaded[i];
			}
		}
		for (int i = 0; i < N; i++) {
			// Overloaded processor sort
			// Set rendez vous partners
			if ((friend[i] != null) && (rcv[friend[i]] != null)) {
				rendezVous[i] = rcv[friend[i]];
				rendezVousQueue.add(i);
			}
		}
	}


	private void initWorkingArrays() {
		L = new Integer[N];
		rcv = new Integer[N];
		dst = new Integer[N];
		valueUnderloaded = new Integer[N];
		valueOverloaded = new Integer[N];
		friend = new Integer[N];
		rendezVous = new Integer[N];
	}



	private Integer[] rank(Integer[] values, int lowest) {
		Integer[] rank = new Integer[values.length];
		int rankCounter = 0;
		for (int i = lowest; i <= 0; i++) {
			for (int j = 0; j < values.length; j++) {
				if ((values[j] != null) && (values[j] == i)) {
					rank[j] = rankCounter++;
				}
			}
		}
		return rank;
	}


	/**
	 * New StateInformation received. Parse and estimate Worker.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTasks(String)}
	 * 5. {@link #selectTarget()}
	 * 6. {@link #cleanupAfterBalance()}
	 *
	 * @param stateInfo new StateInfo to parse
	 * @return true: LoadBalancing workflow will be started
	 * false: Just parse the message
	 */
	@Override
	protected boolean parseIncomingStateInfo(StateInfo stateInfo) {
		String workerName = stateInfo.getSource().getName();
		if (!workersMap.containsKey(workerName)) {
			workersMap.put(workerName, N++);
			workersList.add(workerName);
		}
		int workerId = workersMap.get(workerName);
		stateInfoMap.put(workerId, stateInfo);

		if (N >= 2) {
			involedLBHosts.remove(stateInfo.getSource().getName());
			if (involedLBHosts.size() == 0) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns how many (re)balance actions are necessary.
	 *
	 * @return	x  > 0: workflow will go ahead and call selectSource(), selectTasks() and selectTarget() x times.
	 *			x <= 0: workflow will be stopped
	 */
	@Override
	protected int needOfBalance() {
		return rendezVousQueue.size();
	}


	/**
	 * Select a target worker to move the selected task.
	 *
	 * @return hostname of target worker.
	 */
	@Override
	protected String selectTarget() {
		int k = rendezVousQueue.poll();
		String target = workersList.get(rendezVous[k]);
		involedLBHosts.add(target);
		return target;
	}

	/**
	 * Returns a list of task ids to move.
	 *
	 * @param source Source worker hostname.
	 * @return List of Tasks to move.
	 */
	@Override
	protected List<Integer> selectTasks(String source) {
		List<Integer> tasks = new LinkedList<>();
		int k = rendezVousQueue.peek();
		int numberOfTasks = (L[k] - L[rendezVous[k]]) / 2;

		try {
			TasksInfo ti = requestTasksInfo(source);
			int prio = Job.getHighestActivePrio();
			int len = ti.getTasks(prio).size();
			if (len > numberOfTasks) {
				for (int i = 1; i <= numberOfTasks; i++) {
					tasks.add(ti.getTasks().get(len - i).getTaskId());
				}
			} else {
				// remove source and target from list
				if (involedLBHosts.size() > 0) {
					involedLBHosts.removeLast();
				}
				if (involedLBHosts.size() > 0) {
					involedLBHosts.removeLast();
				}
			}
		} catch (HostFailureException | NativeException | TimeoutException | TransferFailureException e) {
			e.printStackTrace();
		}

		// Update StateInfo
		/*
		double srcQueueLength = stateInfoMap.get(k).getMetricValue(Metric.QUEUE_LENGTH);
		double dstQueueLength = stateInfoMap.get(rendezVous[k]).getMetricValue(Metric.QUEUE_LENGTH);
		srcQueueLength -= numberOfTasks;
		dstQueueLength += numberOfTasks;
		stateInfoMap.get(k).setMetricValue(Metric.QUEUE_LENGTH, srcQueueLength);
		stateInfoMap.get(rendezVous[k]).setMetricValue(Metric.QUEUE_LENGTH, dstQueueLength);
		*/
		return tasks;
	}

	/**
	 * Returns the source (overloaded) worker.
	 *
	 * @return Hostname of worker.
	 */
	@Override
	protected String selectSource() {
		int k = rendezVousQueue.peek();
		String source = workersList.get(k);
		involedLBHosts.add(source);
		return source;
	}

	/**
	 * Method lbActivator (LoadBalancing Activator) will be called every "interval" seconds.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTasks(String)}
	 * 5. {@link #selectTarget()}
	 * 6. {@link #cleanupAfterBalance()}
	 *
	 * @return true: LoadBalancing workflow will be started
	 * false: Just parse the message
	 */
	@Override
	protected boolean lbActivator() {
		return false;
	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}
}
