package at.enfilo.lbsim.algos.global;

import at.enfilo.lbsim.model.process.LoadBalancer;
import at.enfilo.lbsim.model.task.AbstractTask;
import at.enfilo.lbsim.model.task.Job;
import at.enfilo.lbsim.model.task.StateInfo;
import at.enfilo.lbsim.model.task.TasksInfo;
import org.simgrid.msg.*;

import java.util.LinkedList;
import java.util.List;


public class GlobalLoadBalancer extends LoadBalancer {
	private LoadVector loadVector;
	private int loadThreshold;

	public GlobalLoadBalancer(Host host, String name, String[] args) {
		super(host, name, args);
	}

	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *             already parsed and pruned.
	 */
	@Override
	protected void setupFurtherArgs(String[] args) {
		if (args.length != 1) {
			throw new RuntimeException("GlobalLoadBalancer needs one argument: load threshold");
		}

		loadThreshold = Integer.parseInt(args[0]);
	}

	/**
	 * Initialize scheduler. Will be called after setupFurtherArgs and before start listening.
	 */
	@Override
	protected void init() {

	}


	/**
	 * Received a message / task, type CUSTOM, from another process.
	 * This method is called by receiving to parse the message.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTarget()}
	 * 5. {@link #selectTasks(String)}
	 *
	 * @param custom Message to parse.
	 * @return true: load balancing workflow will be started.
	 * false: Just parse the message.
	 */
	@Override
	protected boolean parseIncomingCustomMessage(AbstractTask custom) {
		if (custom instanceof LoadVector) {
			loadVector = LoadVector.class.cast(custom);
		}
		return true;
	}


	/**
	 * Estimate workers. Information received with {@link #parseIncomingCustomMessage(AbstractTask)} or
	 * {@link #parseIncomingStateInfo(StateInfo)}.
	 * <p>
	 * This method is the first step of the LoadBalancing Workflow. Next step: {@link #needOfBalance()}.
	 */
	@Override
	protected void estimateWorkers() {

	}


	/**
	 * New StateInformation received. Parse and estimate Worker.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTarget()}
	 * 5. {@link #selectTasks(String)}
	 *
	 * @param stateInfo new StateInfo to parse
	 * @return true: LoadBalancing workflow will be started
	 * false: Just parse the message
	 */
	@Override
	protected boolean parseIncomingStateInfo(StateInfo stateInfo) {
		// Ignore stateInfo messages
		return false;
	}


	/**
	 * Returns how many (re)balance actions are necessary.
	 *
	 * @return	x  > 0: workflow will go ahead and call selectSource(), selectTasks() and selectTarget() x times.
	 *			x <= 0: workflow will be stopped
	 */
	@Override
	protected int needOfBalance() {
		if (loadVector == null) {
			return 0;
		}

		int thisLoad = loadVector.getLoad(getHost().getName());
		int lowestLoad = loadVector.getLowestLoad();

		if (lowestLoad + loadThreshold <= thisLoad) {
			return 1;
		}
		return 0;
	}


	/**
	 * Select a target worker to move the selected task.
	 *
	 * @return hostname of target worker.
	 */
	@Override
	protected String selectTarget() {
		return loadVector.getWorkerWithLowestLoad();
	}


	/**
	 * Returns a list of task ids to move.
	 *
	 * @param source Source worker hostname.
	 * @return List of Tasks to move.
	 */
	@Override
	protected List<Integer> selectTasks(String source) {
		List<Integer> tasks = new LinkedList<>();
		try {
			TasksInfo ti = requestTasksInfo(getHost().getName());
			int nrOfTasks = ti.getNumberOfTasks(Job.getHighestActivePrio());
			if (nrOfTasks > 0) {
				tasks.add(ti.getTasks(Job.getHighestActivePrio()).get(nrOfTasks - 1).getTaskId());
			}
		} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
			e.printStackTrace();
		}

		return tasks;
	}

	/**
	 * Returns the source (overloaded) worker.
	 *
	 * @return Hostname of worker.
	 */
	@Override
	protected String selectSource() {
		return getHost().getName();
	}


	/**
	 * Method will be called after advising selected workers to balance load (tasks).
	 * Its the last step of LoadBalancing Workflow to clean up all temporary information.
	 */
	@Override
	protected void cleanupAfterBalance() {
		loadVector = null; // Wait for next
	}


	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}


	/**
	 * Method lbActivator (LoadBalancing Activator) will be called every "interval" seconds.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTarget()}
	 * 5. {@link #selectTasks(String)}
	 * 6. {@link #cleanupAfterBalance()}
	 *
	 * @return true: LoadBalancing workflow will be started
	 * false: Just parse the message
	 */
	@Override
	protected boolean lbActivator() {
		return false;
	}
}
