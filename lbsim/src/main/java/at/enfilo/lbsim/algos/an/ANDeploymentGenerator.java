package at.enfilo.lbsim.algos.an;

import java.util.*;
import java.util.stream.Collectors;

public class ANDeploymentGenerator {
	public static void main(String[] args) {
		ArrayList<String> nodes = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			nodes.add("workera" + i);
		}
		for (int i = 0; i < 10; i++) {
			nodes.add("workerb" + i);
		}
//		for (int i = 0; i < 100; i++) {
//			nodes.add("wb" + i);
//		}
//		for (int i = 0; i < 100; i++) {
//			nodes.add("wc" + i);
//		}
//		for (int i = 0; i < 100; i++) {
//			nodes.add("wd" + i);
//		}
//		for (int i = 0; i < 100; i++) {
//			nodes.add("we" + i);
//		}


		ArrayList<Integer> masters = new ArrayList<>();
		HashMap<Integer, List<String>> slavesToMasters = new HashMap<>();

		for (int i = 0; i < nodes.size(); i++) {
			slavesToMasters.put(i, new LinkedList<>());
		}

		int domainSize = 3;
		int[] drifts = {0,2};

		for (int drift : drifts) {
			String master = null;
			for (int i = 0; i < nodes.size(); i++) {
				if (((i + drift) % domainSize) == 0) {
					master = nodes.get(i);
					masters.add(i);
				}
				if (master != null) {
					slavesToMasters.get(i).add(master);
				}
			}
		}

		List<Integer> sortedMasters = masters.stream().sorted().collect(Collectors.toList());

		int n = 0;
		int nrMasters = masters.size();
		while (slavesToMasters.get(n).size() != drifts.length) {
			int diff = drifts.length - slavesToMasters.get(n).size();
			for (int j = 1; j <= diff; j++) {
				slavesToMasters.get(n).add(nodes.get(sortedMasters.get(nrMasters - j)));
			}
			n++;
		}

		int pre = sortedMasters.size() - 1;
		int post = 1;
		for (int i = 0; i < sortedMasters.size(); i++) {
			System.out.println(
					"<process host=\"" + nodes.get(sortedMasters.get(i)) + "\" function=\"at.enfilo.lbsim.algos.an.ANMaster\">\n" +
					"	<argument value=\"0.3\"/><!-- threshold -->\n" +
					"	<argument value=\"" + nodes.get(sortedMasters.get(pre)) + "," + nodes.get(sortedMasters.get(post)) + "\"/><!-- other masters -->\n" +
					//"	<argument value=\"\"/><!-- other masters -->\n" +
					"</process>");
			post = i + 1;
			if (post >= sortedMasters.size()) {
				post = 0;
			}
			pre = i;
		}

		System.out.println();

		for (Map.Entry<Integer, List<String>> slave : slavesToMasters.entrySet()) {
			System.out.println(
					"<process host=\"" + nodes.get(slave.getKey()) + "\" function=\"at.enfilo.lbsim.algos.an.ANStateInfoProvider\">\n" +
					"	<argument value=\"" + slave.getValue().stream().collect(Collectors.joining(",")) +  "\"/>\n" +
					"</process>");
		}

//		int domainSize = 5;
//		String pre = "";
//		String post = "";
//		for (int i = (domainSize / 2); i < nodes.size(); i += domainSize - 1) {
//			if (i + domainSize < nodes.size()) {
//				post = "worker" + nodes.get(i + domainSize - 1);
//			} else {
//				post = "worker" + nodes.get(domainSize / 2);
//			}
//			System.out.println("<process host=\"worker" + nodes.get(i) + "\" function=\"at.enfilo.lbsim.algos.an.ANMaster\">\n" +
//					"	<argument value=\"0.3\"/><!-- threshold -->\n" +
//					"	<argument value=\"" + pre + "," + post + "\"/><!-- other masters -->\n" +
//					//"	<argument value=\"\"/><!-- other masters -->\n" +
//					"</process>");
//			for (int j = 0; j < domainSize; j++) {
//				int x = (i - domainSize / 2) + j;
//				System.out.println("<process host=\"worker" + nodes.get(x) + "\" function=\"at.enfilo.lbsim.algos.an.ANStateInfoProvider\">\n" +
//						"	<argument value=\"" + pre + "," + "worker" + nodes.get(i) + "," + post + "\"/>\n" +
//						"</process>");
//			}
//			System.out.println();
//			pre = "worker" + nodes.get(i);
//		}
	}
}
