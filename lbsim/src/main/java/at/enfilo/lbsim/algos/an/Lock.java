package at.enfilo.lbsim.algos.an;

import at.enfilo.lbsim.model.task.AbstractTask;
import at.enfilo.lbsim.model.task.TaskType;

import java.util.List;

public class Lock extends AbstractTask {
	private static final String NAME_PREFIX = "Lock_";
	private static long idCounter = 0;

	private final long id;
	private final String[] lockedNodes;

	public Lock(String[] lockedNodes) {
		this(idCounter++, lockedNodes);
	}

	private Lock(long id, String[] lockedNodes) {
		super(TaskType.CUSTOM, NAME_PREFIX + id, 0, (lockedNodes.toString().getBytes().length + AbstractTask.INT_SIZE));
		this.id = id;
		this.lockedNodes = lockedNodes;
	}

	boolean isNodeLocked(String node) {
		for (String lock : lockedNodes) {
			if (lock.equals(node)) {
				return true;
			}
		}
		return false;
	}

	long getId() {
		return id;
	}

	UnLock createUnlock() {
		return new UnLock(this.id);
	}

	@Override
	protected Lock clone() throws CloneNotSupportedException {
		return new Lock(this.id, lockedNodes.clone());
	}
}
