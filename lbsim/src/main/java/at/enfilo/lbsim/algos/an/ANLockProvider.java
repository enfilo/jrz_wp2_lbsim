package at.enfilo.lbsim.algos.an;

import at.enfilo.lbsim.model.process.BaseProcess;
import at.enfilo.lbsim.model.process.LoadBalancer;
import at.enfilo.lbsim.model.process.ProcessHelper;
import at.enfilo.lbsim.model.task.AbstractTask;
import at.enfilo.lbsim.model.task.StateInfo;
import at.enfilo.lbsim.model.task.TaskType;
import org.simgrid.msg.Host;

import java.util.LinkedList;
import java.util.List;

public class ANLockProvider extends BaseProcess {

	private final List<Lock> locks;
	private final Object syncLock;

	public ANLockProvider(Host host, String name, String[] args) {
		super(host, name, args);
		locks = new LinkedList<>();
		syncLock = new Object();
	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() {

	}

	/**
	 * Setup process.
	 *
	 * @param args Arguments from deployment.xml
	 * @return Name of own Mailbox to listen. If null is returned no listen.
	 */
	@Override
	protected String setup(String[] args) {
		return ProcessHelper.constructMailboxName(this.getClass(), getHost().getName());
	}


	@Override
	protected void parseAbstractTask(TaskType type, AbstractTask custom) {
		if (custom instanceof Lock) {
			Lock lock = Lock.class.cast(custom);
			synchronized (syncLock) {
				locks.add(lock);
			}
		}
		if (custom instanceof UnLock) {
			UnLock unlock = UnLock.class.cast(custom);
			synchronized (syncLock) {
				locks.removeIf(lock -> (lock.getId() == unlock.getLockId()));
			}
		}
	}



	/**
	 * Returns true if node is locked.
	 * @param node	node/host to prove.
	 * @return	true if given node/host is locked.
	 */
	public boolean isLocked(String node) {
		synchronized (syncLock) {
			for (Lock lock : locks) {
				if (lock.isNodeLocked(node)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {

	}
}
