package at.enfilo.lbsim.algos.jppf;

import at.enfilo.lbsim.model.process.InitialScheduler;
import at.enfilo.lbsim.model.process.ProcessHelper;
import at.enfilo.lbsim.model.process.StateInfoProvider;
import at.enfilo.lbsim.model.statistics.MsgStatistics;
import at.enfilo.lbsim.model.task.Command;
import at.enfilo.lbsim.model.task.Metric;
import at.enfilo.lbsim.model.task.Order;
import at.enfilo.lbsim.model.task.StateInfo;
import at.enfilo.lbsim.model.util.RandomUtil;
import org.simgrid.msg.*;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class ProportionalScheduler extends InitialScheduler {
	private final static double WAIT_FOR_PROCESS_STARTUP = 0.01;

	private float[] workerSpeed;
	private float[] workerRate;
	private final Queue<String> taskAssignQueue;
	private final Random rnd;

	public ProportionalScheduler(Host host, String name, String[] args) {
		super(host, name, args);
		taskAssignQueue = new LinkedList<>();
		rnd = RandomUtil.getInstance();
	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() { }

	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *             already parsed and pruned.
	 */
	@Override
	protected void setupFurtherArgs(String[] args) { }


	/**
	 * Select next worker to schedule task to it.
	 *
	 * @return Hostname of worker.
	 */
	@Override
	protected String selectWorker() {
		return taskAssignQueue.poll();
	}


	/**
	 * Initialize scheduler.
	 */
	@Override
	protected void init() {
		workerSpeed = new float[neighbors.length];

		try {
			waitFor(WAIT_FOR_PROCESS_STARTUP);
		} catch (HostFailureException e) {
			e.printStackTrace();
		}

		Order order = new Order(Command.REQUEST_STATE);
		for (String worker : neighbors) {
			try {
				// Statistics
				MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), order.getType().name(), order.getMessageSize());

				order.send(ProcessHelper.constructMailboxName(StateInfoProvider.class, worker));
			} catch (TransferFailureException | HostFailureException | NativeException | TimeoutException e) {
				e.printStackTrace();
			}
		}
		Msg.verb("Init prop done");
	}

	/**
	 * Handle incoming StateInfo task/message.
	 *
	 * @param si StateInfo to handle.
	 */
	@Override
	protected void parseStateInfo(StateInfo si) {
		int i = 0;
		for (String worker : neighbors) {
			if (si.getSource().getName().equals(worker)) {
				// Assign to every work a speed value: #cores * flops
				workerSpeed[i] = (float) (si.getMetricValue(Metric.CPU_CORES) * si.getMetricValue(Metric.CPU_SPEED));
				return;
			}
			i++;
		}
	}


	/**
	 * This method is only performed if {@link this.waitForSubmit} flag is true.
	 * <p>
	 * Method is called while submitting job and before start scheduling tasks by selecting workers.
	 * After this method {@method selectWorker()} is called for every task in queue.
	 *
	 * @param numberOfTasks Number of tasks to schedule.
	 */
	@Override
	protected void prepareForSubmit(int numberOfTasks) {
		// Estimate workers
		if (workerRate == null) {
			workerRate = new float[neighbors.length];
			double totalSpeed = 0;
			for (double speed : workerSpeed) {
				totalSpeed += speed;
			}
			for (int i = 0; i < neighbors.length; i++) {
				workerRate[i] = (float) (workerSpeed[i] / totalSpeed);
			}
		}

		taskAssignQueue.clear();
		// Define how many tasks per worker according to their speed
		for (int i = 0; i < neighbors.length; i++) {
			int tasks = Math.round(workerRate[i] * numberOfTasks);
			for (int j = 0; j < tasks; j++) {
				taskAssignQueue.add(neighbors[i]);
			}
		}
		// Fill up the queue randomly to exact size
		// Through round errors there is a mismatch between assignments and number of tasks
		while (taskAssignQueue.size() < numberOfTasks) {
			taskAssignQueue.add(neighbors[rnd.nextInt(neighbors.length)]);
		}
	}
}
