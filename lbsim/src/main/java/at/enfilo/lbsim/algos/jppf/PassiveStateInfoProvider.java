package at.enfilo.lbsim.algos.jppf;

import at.enfilo.lbsim.model.process.SourceEvent;
import at.enfilo.lbsim.model.process.StateInfoProvider;
import at.enfilo.lbsim.model.process.Worker;
import at.enfilo.lbsim.model.task.TasksInfo;
import org.simgrid.msg.Host;


public class PassiveStateInfoProvider extends StateInfoProvider {

	public PassiveStateInfoProvider(Host host, String name, String[] args) {
		super(host, name, args);
	}

	/**
	 * Setup and parse arguments.
	 *
	 * @param args Arguments from deployment.xml.
	 */
	@Override
	protected void setupArguments(String[] args) { }

	/**
	 * Update from {@link Worker} about the actual tasks.
	 *
	 * @param ti All tasks at local {@link Worker} process.
	 */
	@Override
	public void update(TasksInfo ti, SourceEvent event) { }

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() { }

	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() { }
}
