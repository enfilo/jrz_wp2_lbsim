package at.enfilo.lbsim.algos.global;

import at.enfilo.lbsim.model.task.AbstractTask;
import at.enfilo.lbsim.model.task.TaskType;
import at.enfilo.lbsim.model.util.RandomUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;


/**
 * LoadVector, holds load information of all workers.
 *
 * Load == Queue Length.
 *
 * LIC broadcast this to all workers.
 */
public class LoadVector extends AbstractTask {
	private HashMap<String, Integer> load;
	private int nrOfWorkers;

	public LoadVector(int nrOfWorkers) {
		super(TaskType.CUSTOM, LoadVector.class.getName(), 0,
				(nrOfWorkers * AbstractTask.INT_SIZE) * 2);
		// Optimal task size: identification and load info for every worker
		// identification by an int and load information ist also an int

		this.nrOfWorkers = nrOfWorkers;
		load = new HashMap<>();
	}

	public int getLoad(String worker) {
		if (load.containsKey(worker)) {
			return load.get(worker);
		}
		return -1;
	}

	public void setLoad(String worker, int load) {
		this.load.put(worker, load);
	}

	@Override
	public LoadVector clone() {
		LoadVector clone = new LoadVector(nrOfWorkers);
		clone.load = (HashMap<String, Integer>) load.clone();
		return clone;
	}

	/**
	 * Returns the lowest load value.
	 * Load in this case is the length of queue.
	 *
	 * @return load value.
	 */
	public int getLowestLoad() {
		return load.values().stream().min(Integer::compare).get();
	}


	/**
	 * Returns the hostname of worker with the lowest load (= shortest queue)
	 *
	 * If more worker have the same load, one will randomly picked.
	 *
	 * @return worker hostname.
	 */
	public String getWorkerWithLowestLoad() {
		int lowest = getLowestLoad();

		List<String> workers = load.entrySet()
				.stream()
				.filter(entry -> entry.getValue() == lowest)
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
		switch (workers.size()) {
			case 0:
				return null;
			case 1:
				return workers.get(0);
			default:
				Random rnd = RandomUtil.getInstance();
				return workers.get(rnd.nextInt(workers.size()));
		}
	}

	/**
	 * Returns the number of load information already in the load vector.
	 * @return
	 */
	public int getNrOfLoadInformations() {
		return load.size();
	}
}
