package at.enfilo.lbsim.algos;

import at.enfilo.lbsim.model.process.InitialScheduler;
import at.enfilo.lbsim.model.task.StateInfo;
import org.simgrid.msg.Host;

/**
 * A simple RoundRobin implementation
 */
public class RRInitialScheduler extends InitialScheduler {
	private int next;

	public RRInitialScheduler(Host host, String name, String[] args) {
		super(host, name, args);
	}

	/**
	 * Initialize scheduler.
	 */
	@Override
	protected void init() {
		next = neighbors.length;
	}

	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *             already parsed and pruned.
	 */
	@Override
	protected void setupFurtherArgs(String[] args) { }

	/**
	 * This method is only performed if {@link this.waitForSubmit} flag is true.
	 * <p>
	 * Method is called while submitting job and before start scheduling tasks by selecting workers.
	 * After this method {@method selectWorker()} is called for every task in queue.
	 *
	 * @param numberOfTasks Number of tasks to schedule.
	 */
	@Override
	protected void prepareForSubmit(int numberOfTasks) { }

	/**
	 * Handle incoming StateInfo task/message.
	 *
	 * @param si StateInfo to handle.
	 */
	@Override
	protected void parseStateInfo(StateInfo si) { }

	/**
	 * Select next worker to schedule task to it.
	 *
	 * @return Hostname of worker.
	 */
	@Override
	protected String selectWorker() {
		next = (next + 1) % neighbors.length;
		return this.neighbors[next];
	}

	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}

	/**
	 * Activates this process.
	 * Method will be called in periodic manner every "interval" seconds.
	 * The interval is specified by {@code periodicInterval()}
	 */
	@Override
	protected void periodicActivator() { }
}
