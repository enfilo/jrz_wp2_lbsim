package at.enfilo.lbsim.algos.an;

import at.enfilo.lbsim.model.process.LoadBalancer;
import at.enfilo.lbsim.model.process.ProcessHelper;
import at.enfilo.lbsim.model.statistics.MsgStatistics;
import at.enfilo.lbsim.model.task.*;
import at.enfilo.lbsim.model.task.Task;
import org.simgrid.msg.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ANMaster extends LoadBalancer {

	private final HashMap<String, Double> loadSlaves;
	private double loadMaster;
	private double loadAverage;
	private double threshold;
	private double thresholdSender;
	private double thresholdReceiver;
	private String masterHost;
	private String[] otherMasters;

	private ANLockProvider lockProvider;
	private Lock currentLock;
	private String balancingSender;
	private String balancingReceiver;

	public ANMaster(Host host, String name, String[] args) {
		super(host, name, args);
		loadSlaves = new HashMap<>();
		loadMaster = -1;
	}

	/**
	 * Setup further arguments from deployment.xml.
	 *
	 * @param args Arguments from deployment.xml. First two arguments (list of neighbors and waitForSubmit)
	 *             already parsed and pruned.
	 */
	@Override
	protected void setupFurtherArgs(String[] args) {
		if (args.length != 2) {
			throw new RuntimeException("ANMaster needs two Arguments: percentage of thresholds and a list of other master hosts");
		}

		threshold = Double.parseDouble(args[0]);
		otherMasters = args[1].split(",");
	}

	/**
	 * Initialize scheduler. Will be called after setupFurtherArgs and before start listening.
	 */
	@Override
	protected void init() {
		masterHost = getHost().getName();
		lockProvider = new ANLockProvider(getHost(), ANLockProvider.class.getName(), new String[]{});
		try {
			lockProvider.start();
		} catch (HostNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Received a message / task, type CUSTOM, from another process.
	 * This method is called by receiving to parse the message.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTasks(String)}
	 * 5. {@link #selectTarget()}
	 * 6. {@link #cleanupAfterBalance()}
	 *
	 * @param custom Message to parse.
	 * @return true: load balancing workflow will be started.
	 * false: Just parse the message.
	 */
	@Override
	protected boolean parseIncomingCustomMessage(AbstractTask custom) {
		/*
		if (custom instanceof Lock) {
			Lock lock = Lock.class.cast(custom);
			locks.add(lock);
		}
		if (custom instanceof UnLock) {
			UnLock unlock = UnLock.class.cast(custom);
			locks.removeIf(lock -> (lock.getId() == unlock.getLockId()));
		}
		*/
		return false;
	}

	/**
	 * Method will be called after advising selected workers to balance load (tasks).
	 * Its the last step of LoadBalancing Workflow to clean up all temporary information.
	 */
	@Override
	protected void cleanupAfterBalance() {
		/*
		if (currentLock != null) {
			broadcastUnLock(currentLock.createUnlock());
			currentLock = null;
		}
		*/
	}

	/**
	 * Estimate workers. Information received with {@link #parseIncomingCustomMessage(AbstractTask)} or
	 * {@link #parseIncomingStateInfo(StateInfo)}.
	 * <p>
	 * This method is the first step of the LoadBalancing Workflow. Next step: {@link #needOfBalance()}.
	 */
	@Override
	protected void estimateWorkers() {
		double loadSum = loadMaster;
		loadSum += loadSlaves.values().stream().mapToDouble(Double::doubleValue).sum();
		loadAverage = loadSum / (1 + loadSlaves.size());
		thresholdSender = loadAverage * (1 + threshold);
		thresholdReceiver = loadAverage * (1 - threshold);
	}

	/**
	 * New StateInformation received. Parse and estimate Worker.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTasks(String)}
	 * 5. {@link #selectTarget()}
	 * 6. {@link #cleanupAfterBalance()}
	 *
	 * @param stateInfo new StateInfo to parse
	 * @return true: LoadBalancing workflow will be started
	 * false: Just parse the message
	 */
	@Override
	protected boolean parseIncomingStateInfo(StateInfo stateInfo) {
		double load = stateInfo.getMetricValue(Metric.QUEUE_LENGTH);
		String source = stateInfo.getSource().getName();
		if (source.equals(masterHost)) {
			// Master node
			loadMaster = load;
		} else {
			// A slave node
			loadSlaves.put(source, load);
		}

		// only if there are more then two neighbour (slave) nodes its useful to start the workflow.
		if ((loadMaster >= 0) && (loadSlaves.size() >= 1)) {
			if ((balancingSender != null) && balancingSender.equals(stateInfo.getSource().getName())) {
				balancingSender = null;
			}
			if ((balancingReceiver != null) && balancingReceiver.equals(stateInfo.getSource().getName())) {
				balancingReceiver = null;
			}
			// Start workflow only if all states from last load balancing actions received.
			if (balancingSender == null && balancingReceiver == null) {
				if (currentLock != null) {
					// A lock is active from last load balancing action. all states are received, unlock the nodes
					broadcastUnLock(currentLock.createUnlock());
					currentLock = null;
				}
				// Return true to start workflow
				return true;
			}
		}
		return false;

	}


	/**
	 * Returns how many (re)balance actions are necessary.
	 *
	 * @return	x  > 0: workflow will go ahead and call selectSource(), selectTasks() and selectTarget() x times.
	 *			x <= 0: workflow will be stopped
	 */
	@Override
	protected int needOfBalance() {
		// If master load is over/under threshold, a balance action is required
		if ((loadMaster < thresholdReceiver) && overloadedHostsAvail()) {
			return 1;
		}
		if ((loadMaster > thresholdSender) && underloadedHostsAvail()) {
			return 1;
		}
		// If a slave load is over/under threshold, a balance action is required
		if (overloadedHostsAvail() && underloadedHostsAvail()) {
			return 1;
		}
		return 0;
	}


	/**
	 * Returns true if at least one host ist underloaded.
	 * @return	true if one host is underloaded.
	 */
	private boolean underloadedHostsAvail() {
		for (double load : loadSlaves.values()) {
			if (load < thresholdReceiver) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns true if at least one host ist overloaded.
	 * @return	true if one host is overloaded.
	 */
	private boolean overloadedHostsAvail() {
		for (double load : loadSlaves.values()) {
			if (load > thresholdSender) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Select a target worker to move the selected task.
	 *
	 * @return hostname of target worker.
	 */
	@Override
	protected String selectTarget() {
		if (loadMaster < thresholdReceiver) {
			balancingReceiver = masterHost;
		} else {
			Map.Entry<String, Double> dst = loadSlaves.entrySet()
					.stream()
					.min((o1, o2) -> Double.compare(o1.getValue(), o2.getValue()))
					.get();
			balancingReceiver = dst.getKey();
		}
		if (lockProvider.isLocked(balancingReceiver)) {
			balancingReceiver = null;
		} else {
			currentLock = new Lock(new String[] {balancingReceiver, balancingSender});
			broadcastLock(currentLock);
		}
		return balancingReceiver;
	}

	/**
	 * Returns a list of task ids to move.
	 *
	 * @param source Source worker hostname.
	 * @return List of Tasks to move.
	 */
	@Override
	protected List<Integer> selectTasks(String source) {
		List<Integer> taskIds = new LinkedList<>();

		// Calculate how much to balance
		int numberOfTasks = 0;
		if (source.equals(masterHost)) {
			numberOfTasks = new Double(loadMaster - loadAverage).intValue();
		} else {
			numberOfTasks = new Double(loadSlaves.get(source) - loadAverage).intValue();
		}
		if (numberOfTasks == 0) {
			numberOfTasks = 1;
		}

		// Select task ids
		try {
			TasksInfo ti = requestTasksInfo(source);
			List<Task> tasks = ti.getTasks(Job.getHighestActivePrio());
			if (tasks.size() > numberOfTasks) {
				// Pick the last tasks from list
				for (int i = 1; i <= numberOfTasks; i++) {
					taskIds.add(tasks.get(tasks.size() - i).getTaskId());
				}
			}
			if (taskIds.size() == 0) {
				balancingReceiver = null;
				balancingSender = null;
			}
		} catch (HostFailureException | NativeException | TransferFailureException | TimeoutException e) {
			e.printStackTrace();
		}
		return taskIds;
	}

	/**
	 * Returns the source (overloaded) worker.
	 *
	 * @return Hostname of worker.
	 */
	@Override
	protected String selectSource() {
		if (loadMaster > thresholdSender) {
			balancingSender = masterHost;
		} else {
			Map.Entry<String, Double> src = loadSlaves.entrySet()
												.stream()
												.max((o1, o2) -> Double.compare(o1.getValue(), o2.getValue()))
												.get();
			balancingSender = src.getKey();
		}
		if (lockProvider.isLocked(balancingSender)) {
			balancingSender = null;
		}
		return balancingSender;
	}


	/**
	 * Broadcast a lock message to other master nodes.
	 * @param lock	lock  object.
	 */
	private void broadcastLock(Lock lock) {
		for (String otherMaster : otherMasters) {
			try {
				// Statistics
				MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), lock.getType().name(), lock.getMessageSize());

				lock.clone().dsend(ProcessHelper.constructMailboxName(ANLockProvider.class, otherMaster));
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
	}


	/**
	 * Broadcast a unlock message to other master nodes.
	 * @param unlock	unlock object.
	 */
	private void broadcastUnLock(UnLock unlock) {
		for (String otherMaster : otherMasters) {
			try {
				// Statistics
				MsgStatistics.sendMessage(getHost().getName(), this.getClass().getName(), unlock.getType().name(), unlock.getMessageSize());

				unlock.clone().dsend(ProcessHelper.constructMailboxName(ANLockProvider.class, otherMaster));
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
	}


	/**
	 * Define a periodic interval for this process.
	 * Method {@code periodicActivator()} will be called in periodic manner every "interval" seconds.
	 *
	 * @return Interval in seconds. If interval is 0.0 or lower, no periodic interval will be started.
	 */
	@Override
	protected double periodicInterval() {
		return 0;
	}


	/**
	 * Method lbActivator (LoadBalancing Activator) will be called every "interval" seconds.
	 * If this method returned true, the LoadBalancing Workflow will be initiated:
	 * 1. {@link #estimateWorkers()}
	 * 2. {@link #needOfBalance()}
	 * 3. {@link #selectSource()}
	 * 4. {@link #selectTarget()}
	 * 5. {@link #selectTasks(String)}
	 * 6. {@link #cleanupAfterBalance()}
	 *
	 * @return true: LoadBalancing workflow will be started
	 * false: Just parse the message
	 */
	@Override
	protected boolean lbActivator() {
		return false;
	}


	/**
	 * Shutdown handler.
	 */
	@Override
	protected void shutdown() {
		Order shutdown = new Order(Command.SHUTDOWN);
		shutdown.dsend(ProcessHelper.constructMailboxName(ANLockProvider.class, masterHost));
		try {
			waitFor(1.0);
		} catch (HostFailureException e) {
			e.printStackTrace();
		}
	}
}
