package at.enfilo.lbsim.algos.an;

import at.enfilo.lbsim.model.task.AbstractTask;
import at.enfilo.lbsim.model.task.TaskType;

import java.util.List;

public class UnLock extends AbstractTask {
	private static final String NAME_PREFIX = "UnLock_";
	private static long idCounter = 0;

	private final long lockId;

	public UnLock(long lockId) {
		super(TaskType.CUSTOM, NAME_PREFIX + (idCounter++), 0, AbstractTask.INT_SIZE);
		this.lockId = lockId;
	}

	long getLockId() {
		return lockId;
	}

	@Override
	protected UnLock clone() throws CloneNotSupportedException {
		return new UnLock(this.lockId);
	}
}
