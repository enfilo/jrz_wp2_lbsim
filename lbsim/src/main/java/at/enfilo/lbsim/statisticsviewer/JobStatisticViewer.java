package at.enfilo.lbsim.statisticsviewer;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.opencsv.CSVReader;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.category.SlidingCategoryDataset;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class JobStatisticViewer extends JFrame {
	private static final String[] TASK_ROW_KEYS = new String[]{"", "Scheduling", "Queued", "Fetching Data", "Executing", "Store Result"};
	private static final String[] JOB_OVERVIEW_ROW_KEYS = new String[]{"", "Waiting", "Running"};
	private static final String[] JOB_DETAIL_ROW_KEYS = new String[]{
			"Complete Schedule Time", "Complete Queue Time", "Complete Fetch-Data Time",
			"Complete Execute Time", "Complete Store-Result Time"};
	private static final Color C_TRANSPARENT = new Color(0, 0, 0, 0);
	private static final Color C_SCHEDULING = new Color(0, 0, 0, 80);
	private static final Color C_QUEUEING = new Color(0, 0, 0, 120);
	private static final Color C_FETCHING = new Color(80, 80, 250, 255);
	private static final Color C_EXECUTING = new Color(220, 100, 100, 255);
	private static final Color C_STORING = new Color(80, 150, 80, 255);

	private JPanel pnlRoot;
	private JPanel pnlFilter;
	private JLabel lblJobs;
	private JPanel pnlJobs;
	private JPanel pnlTaskChart;
	private JPanel pnlJobCharts;
	private JPanel pnlJobOverviewChart;
	private JPanel pnlJobDetailChart;
	private JPanel pnlTaskScroller;
	private JButton btnNext;
	private JButton btnPrevious;
	private JSpinner spnMaxColumns;
	private JButton btnClose;
	private JButton btnExport;

	private JFreeChart crtTasks;
	private final DefaultCategoryDataset dataSetAllTasks = new DefaultCategoryDataset();
	private final DefaultCategoryDataset dataSetTasks = new DefaultCategoryDataset();
	private final HashMap<String, List<String>> jobTaskList = new HashMap<>();
	private int taskIndex = 0;
	private int maxColumns = 50;
	private final SlidingCategoryDataset slidingDataSetTasks = new SlidingCategoryDataset(dataSetTasks, taskIndex, maxColumns);


	private JFreeChart crtJobOverview;
	private final DefaultCategoryDataset dataSetJobOverview = new DefaultCategoryDataset();

	private JFreeChart crtJobDetail;
	private final DefaultCategoryDataset dataSetJobDetail = new DefaultCategoryDataset();


	public JobStatisticViewer() {
		super("Job Statistics Viewer");

		setupTaskChart(pnlTaskChart);
		setupJobOverviewChart(pnlJobOverviewChart);
		setupJobDetailChart(pnlJobDetailChart);

		// Show GUI
		setContentPane(pnlRoot);
		pack();
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
		btnPrevious.addActionListener(new ActionListener() {
			/**
			 * Invoked when an action occurs.
			 *
			 * @param e
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (taskIndex >= maxColumns) {
					taskIndex -= maxColumns;
				} else {
					taskIndex = 0;
				}
				slidingDataSetTasks.setFirstCategoryIndex(taskIndex);
				crtTasks.fireChartChanged();
			}
		});
		btnNext.addActionListener(new ActionListener() {
			/**
			 * Invoked when an action occurs.
			 *
			 * @param e
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				if (dataSetAllTasks.getColumnCount() - maxColumns > taskIndex) {
					taskIndex += maxColumns;
				} else {
					taskIndex = dataSetAllTasks.getColumnCount() - maxColumns;
				}
				slidingDataSetTasks.setFirstCategoryIndex(taskIndex);
				crtTasks.fireChartChanged();
			}
		});
		spnMaxColumns.setValue(new Integer(maxColumns));
		spnMaxColumns.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				maxColumns = (Integer) spnMaxColumns.getValue();
				slidingDataSetTasks.setMaximumCategoryCount(maxColumns);
				crtTasks.fireChartChanged();
			}
		});
		btnClose.addActionListener(new ActionListener() {
			/**
			 * Invoked when an action occurs.
			 *
			 * @param e
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnExport.addActionListener(new ActionListener() {
			/**
			 * Invoked when an action occurs.
			 *
			 * @param e
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				PDFExporter.writeChartToPDF(crtTasks, pnlTaskChart.getWidth(), pnlTaskChart.getHeight(), "tasksGraph.pdf");
				PDFExporter.writeChartToPDF(crtJobOverview, pnlJobOverviewChart.getWidth(), pnlJobOverviewChart.getHeight(), "jobOverviewGraph.pdf");
				PDFExporter.writeChartToPDF(crtJobDetail, pnlJobDetailChart.getWidth(), pnlJobDetailChart.getHeight(), "jobDetailGraph.pdf");
			}
		});
	}


	/**
	 * Setup Chart for Tasks.
	 *
	 * @param root JPanel to place the chart.
	 */
	private void setupTaskChart(JPanel root) {
		crtTasks = ChartFactory.createStackedBarChart(
				"Task Statistics",
				"Task",                      // domain axis label
				"Time",                      // range axis label
				slidingDataSetTasks,         // data
				PlotOrientation.HORIZONTAL,  // the plot orientation
				true,                        // include legend
				true,                        // tooltips
				false                        // urls
		);

		CategoryPlot plot = crtTasks.getCategoryPlot();
		StackedBarRenderer renderer = (StackedBarRenderer) plot.getRenderer();
		renderer.setBarPainter(new StandardBarPainter());
		int i = 0;
		renderer.setSeriesPaint(i++, C_TRANSPARENT); // First series is transparent
		renderer.setSeriesPaint(i++, C_SCHEDULING); // Scheduling
		renderer.setSeriesPaint(i++, C_QUEUEING); // Queued
		renderer.setSeriesPaint(i++, C_FETCHING); // Fetch Data
		renderer.setSeriesPaint(i++, C_EXECUTING); // Executing
		renderer.setSeriesPaint(i++, C_STORING); // Store Result

		crtTasks.getCategoryPlot().setBackgroundPaint(Color.white);
		crtTasks.getCategoryPlot().setDomainGridlinePaint(Color.lightGray);
		crtTasks.getCategoryPlot().setRangeGridlinePaint(Color.lightGray);

		ChartPanel cp = new ChartPanel(crtTasks);
		cp.setPreferredSize(new Dimension(500, 500));
		cp.setMaximumDrawWidth(2000);
		cp.setMaximumDrawHeight(1000);
		root.add(cp);
	}


	/**
	 * Setup chart for job overview.
	 *
	 * @param root JPanel to place the chart.
	 */
	private void setupJobOverviewChart(JPanel root) {
		crtJobOverview = ChartFactory.createStackedBarChart(
				"Job Overview",
				"Job",                       // domain axis label
				"Time",                      // range axis label
				dataSetJobOverview,                // data
				PlotOrientation.HORIZONTAL,  // the plot orientation
				true,                        // include legend
				true,                        // tooltips
				false                        // urls
		);
		CategoryPlot plot = crtJobOverview.getCategoryPlot();
		StackedBarRenderer renderer = (StackedBarRenderer) plot.getRenderer();
		renderer.setBarPainter(new StandardBarPainter());
		int i = 0;
		renderer.setSeriesPaint(i++, C_TRANSPARENT); // First series is transparent
		renderer.setSeriesPaint(i++, C_SCHEDULING); // Scheduling or Waiting in this case
		renderer.setSeriesPaint(i++, C_EXECUTING); // Executing, Fetching and Storing

		crtJobOverview.getCategoryPlot().setBackgroundPaint(Color.white);
		crtJobOverview.getCategoryPlot().setDomainGridlinePaint(Color.lightGray);
		crtJobOverview.getCategoryPlot().setRangeGridlinePaint(Color.lightGray);

		ChartPanel cp = new ChartPanel(crtJobOverview);
		cp.setPreferredSize(new Dimension(500, 500));
		cp.setMaximumDrawWidth(2000);
		cp.setMaximumDrawHeight(1000);
		root.add(cp);
	}


	/**
	 * Setup chart for job details.
	 *
	 * @param root JPanel to place the chart.
	 */
	private void setupJobDetailChart(JPanel root) {
		crtJobDetail = ChartFactory.createStackedBarChart(
				"Job Detail (All Tasks)",
				"Job",                       // domain axis label
				"Time",                      // range axis label
				dataSetJobDetail,            // data
				PlotOrientation.HORIZONTAL,  // the plot orientation
				true,                        // include legend
				true,                        // tooltips
				false                        // urls
		);
		CategoryPlot plot = crtJobDetail.getCategoryPlot();
		StackedBarRenderer renderer = (StackedBarRenderer) plot.getRenderer();
		renderer.setBarPainter(new StandardBarPainter());
		int i = 0;
		renderer.setSeriesPaint(i++, C_SCHEDULING);
		renderer.setSeriesPaint(i++, C_QUEUEING);
		renderer.setSeriesPaint(i++, C_FETCHING);
		renderer.setSeriesPaint(i++, C_EXECUTING);
		renderer.setSeriesPaint(i++, C_STORING);

		crtJobDetail.getCategoryPlot().setBackgroundPaint(Color.white);
		crtJobDetail.getCategoryPlot().setDomainGridlinePaint(Color.lightGray);
		crtJobDetail.getCategoryPlot().setRangeGridlinePaint(Color.lightGray);

		ChartPanel cp = new ChartPanel(crtJobDetail);
		cp.setPreferredSize(new Dimension(500, 500));
		cp.setMaximumDrawWidth(2000);
		cp.setMaximumDrawHeight(1000);
		root.add(cp);
	}


	/**
	 * Read task statistics file and build up the chart (data).
	 *
	 * @param taskStatsFile Tasks statistics file.
	 */
	public void showTaskStatistics(String taskStatsFile) {
		pnlJobs.removeAll();
		pnlJobs.setLayout(new BoxLayout(pnlJobs, BoxLayout.PAGE_AXIS));

		double maxRangeValue = 0.0;

		try (CSVReader reader = new CSVReader(new FileReader(taskStatsFile))) {
			String[] line;
			reader.readNext(); // Skip first line
			while ((line = reader.readNext()) != null) {
				// "JobId","TaskId","CreationTime","StartFetchDataTime","StartQueueTime",
				// "EndQueueTime","EndFetchDataTime","StartExecTime","EndExecTime",
				// "StartStoreResultTime","EndStoreResultTime","FinishedTime","MoveCounter"

				int i = 2;
				double creationTime = Double.parseDouble(line[i++]);
				double queueStart = Double.parseDouble(line[i++]);
				double queueEnd = Double.parseDouble(line[i++]);
				double fetchDataStart = Double.parseDouble(line[i++]);
				double fetchDataEnd = Double.parseDouble(line[i++]);
				double execStart = Double.parseDouble(line[i++]);
				double execEnd = Double.parseDouble(line[i++]);
				double storeStart = Double.parseDouble(line[i++]);
				double storeEnd = Double.parseDouble(line[i++]);
				i = 0;
				dataSetTasks.addValue(creationTime, TASK_ROW_KEYS[i++], line[1]);
				dataSetTasks.addValue((queueStart - creationTime), TASK_ROW_KEYS[i++], line[1]);
				dataSetTasks.addValue((queueEnd - queueStart), TASK_ROW_KEYS[i++], line[1]);
				dataSetTasks.addValue((fetchDataEnd - fetchDataStart), TASK_ROW_KEYS[i++], line[1]);
				dataSetTasks.addValue((execEnd - execStart), TASK_ROW_KEYS[i++], line[1]);
				dataSetTasks.addValue((storeEnd - storeStart), TASK_ROW_KEYS[i++], line[1]);
				i = 0;
				dataSetAllTasks.addValue(creationTime, TASK_ROW_KEYS[i++], line[1]);
				dataSetAllTasks.addValue((queueStart - creationTime), TASK_ROW_KEYS[i++], line[1]);
				dataSetAllTasks.addValue((queueEnd - queueStart), TASK_ROW_KEYS[i++], line[1]);
				dataSetAllTasks.addValue((fetchDataEnd - fetchDataStart), TASK_ROW_KEYS[i++], line[1]);
				dataSetAllTasks.addValue((execEnd - execStart), TASK_ROW_KEYS[i++], line[1]);
				dataSetAllTasks.addValue((storeEnd - storeStart), TASK_ROW_KEYS[i++], line[1]);

				if (maxRangeValue <= storeEnd) {
					maxRangeValue = storeEnd;
				}

				final String key = line[0];
				if (!jobTaskList.containsKey(key)) {
					jobTaskList.put(line[0], new LinkedList<>());
					Checkbox checkJob = new Checkbox("Job " + line[0], true);
					checkJob.addItemListener(event -> {
						if (event.getStateChange() == ItemEvent.DESELECTED) {
							// Remove all from job
							jobTaskList.get(key).forEach(dataSetTasks::removeColumn);
						} else if (event.getStateChange() == ItemEvent.SELECTED) {
							// Add all from job
							for (String taskId : jobTaskList.get(key)) {
								for (String rowId : TASK_ROW_KEYS) {
									dataSetTasks.addValue(dataSetAllTasks.getValue(rowId, taskId), rowId, taskId);
								}
							}
						}
						crtTasks.fireChartChanged();
					});
					pnlJobs.add(checkJob);
				}
				jobTaskList.get(key).add(line[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		crtTasks.fireChartChanged();
		crtTasks.getCategoryPlot().getRangeAxis().setRange(0, maxRangeValue);
		pnlJobs.validate();
	}


	/**
	 * Read job statistics file and build up job overview and detail chart.
	 *
	 * @param jobsStatFile Job statistics file.
	 */
	public void showJobStatistics(String jobsStatFile) {
		try (CSVReader reader = new CSVReader(new FileReader(jobsStatFile))) {
			String[] line;
			reader.readNext(); // Skip first line
			while ((line = reader.readNext()) != null) {
				// "JobId","CreationTime","StartTime","EndTime","Tasks",
				// "CompleteScheduleTime","CompleteQueueTime","CompleteFetchDataTime","CompleteExecuteTime","CompleteStoreResultTime"
				int i = 1;
				double creationTime = Double.parseDouble(line[i++]);
				double startTime = Double.parseDouble(line[i++]);
				double endTime = Double.parseDouble(line[i++]);
				int tasks = Integer.parseInt(line[i++]);
				double completeScheduleTime = Double.parseDouble(line[i++]);
				double completeQueueTime = Double.parseDouble(line[i++]);
				double completeFetchDataTime = Double.parseDouble(line[i++]);
				double completeExecuteTime = Double.parseDouble(line[i++]);
				double completeStoreResultTime = Double.parseDouble(line[i++]);

				final String key = line[0];
				i = 0;
				dataSetJobOverview.addValue(creationTime, JOB_OVERVIEW_ROW_KEYS[i++], key);
				dataSetJobOverview.addValue((startTime - creationTime), JOB_OVERVIEW_ROW_KEYS[i++], key);
				dataSetJobOverview.addValue((endTime - startTime), JOB_OVERVIEW_ROW_KEYS[i++], key);
				i = 0;
				dataSetJobDetail.addValue(completeScheduleTime, JOB_DETAIL_ROW_KEYS[i++], key);
				dataSetJobDetail.addValue(completeQueueTime, JOB_DETAIL_ROW_KEYS[i++], key);
				dataSetJobDetail.addValue(completeFetchDataTime, JOB_DETAIL_ROW_KEYS[i++], key);
				dataSetJobDetail.addValue(completeExecuteTime, JOB_DETAIL_ROW_KEYS[i++], key);
				dataSetJobDetail.addValue(completeStoreResultTime, JOB_DETAIL_ROW_KEYS[i++], key);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		crtJobOverview.fireChartChanged();
	}

	public static void main(String[] args) {
		JobStatisticViewer viewer = new JobStatisticViewer();

		String jobsStatsFile = args.length > 1 ? args[0] : "job_stats.csv";
		String tasksStatsFile = args.length > 1 ? args[1] : "task_stats.csv";

		viewer.showTaskStatistics(tasksStatsFile);
		viewer.showJobStatistics(jobsStatsFile);
	}

	{
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
		$$$setupUI$$$();
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer
	 * >>> IMPORTANT!! <<<
	 * DO NOT edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		pnlRoot = new JPanel();
		pnlRoot.setLayout(new GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
		pnlFilter = new JPanel();
		pnlFilter.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		pnlRoot.add(pnlFilter, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		lblJobs = new JLabel();
		lblJobs.setText("Jobs:");
		pnlFilter.add(lblJobs, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		pnlJobs = new JPanel();
		pnlJobs.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
		pnlFilter.add(pnlJobs, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		pnlTaskChart = new JPanel();
		pnlTaskChart.setLayout(new BorderLayout(0, 0));
		pnlRoot.add(pnlTaskChart, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		pnlTaskScroller = new JPanel();
		pnlTaskScroller.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		pnlTaskChart.add(pnlTaskScroller, BorderLayout.SOUTH);
		btnPrevious = new JButton();
		btnPrevious.setText("<");
		pnlTaskScroller.add(btnPrevious);
		spnMaxColumns = new JSpinner();
		spnMaxColumns.setAutoscrolls(true);
		spnMaxColumns.setMinimumSize(new Dimension(60, 26));
		spnMaxColumns.setPreferredSize(new Dimension(60, 26));
		pnlTaskScroller.add(spnMaxColumns);
		btnNext = new JButton();
		btnNext.setText(">");
		pnlTaskScroller.add(btnNext);
		pnlJobCharts = new JPanel();
		pnlJobCharts.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		pnlRoot.add(pnlJobCharts, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		pnlJobOverviewChart = new JPanel();
		pnlJobOverviewChart.setLayout(new BorderLayout(0, 0));
		pnlJobCharts.add(pnlJobOverviewChart, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		pnlJobDetailChart = new JPanel();
		pnlJobDetailChart.setLayout(new BorderLayout(0, 0));
		pnlJobCharts.add(pnlJobDetailChart, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		final JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 0));
		pnlRoot.add(panel1, new GridConstraints(1, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		btnExport = new JButton();
		btnExport.setText("Export charts to PDF");
		panel1.add(btnExport);
		btnClose = new JButton();
		btnClose.setText("Close");
		panel1.add(btnClose);
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return pnlRoot;
	}
}
