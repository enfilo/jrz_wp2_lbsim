package at.enfilo.lbsim.statisticsviewer;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.opencsv.CSVReader;
import org.jfree.chart.*;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class WorkerStatisticsViewer extends JFrame {
	private static final Color C_WORKER_SPEED = new Color(120, 200, 120, 255);
	private static final Color C_TASKS = new Color(120, 120, 220, 255);

	private JPanel pnlRoot;
	private JPanel pnlFilter;
	private JLabel lblQueues;
	private JPanel pnlQueueGraph;
	private JPanel pnlQueues;
	private JPanel pnlWorkers;
	private JPanel pnlWorkerGraphs;
	private JPanel pnlWorkerTaskGraph;
	private JPanel pnlWorkerFlopsGraph;
	private JCheckBox workersCheckBox;
	private JPanel pnlButtons;
	private JButton btnClose;
	private JButton btnExport;

	private JFreeChart crtQueues;
	private final XYSeriesCollection dataSetQueues = new XYSeriesCollection();
	private final HashMap<String, XYSeries> seriesWorkerAllQueues = new HashMap<>();
	private final HashMap<String, XYSeries> seriesWorkerSingleQueue = new HashMap<>();

	private JFreeChart crtWorkerTasks;
	private final DefaultCategoryDataset dataSetWorkerSpeed = new DefaultCategoryDataset();
	private final DefaultCategoryDataset dataSetWorkerTasks = new DefaultCategoryDataset();

	private JFreeChart crtWorkerFlops;
	private final DefaultCategoryDataset dataSetWorkerFlops = new DefaultCategoryDataset();


	public WorkerStatisticsViewer() {
		super("Worker Statistics Viewer");

		setupQueueChart(pnlQueueGraph);
		setupWorkerTaskChart(pnlWorkerTaskGraph);
		setupWorkerFlopsChart(pnlWorkerFlopsGraph);

		// Show GUI
		setContentPane(pnlRoot);
		pack();
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
		workersCheckBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent event) {
				if (event.getStateChange() == ItemEvent.DESELECTED) {
					dataSetQueues.removeAllSeries();
				}
				crtQueues.fireChartChanged();
			}
		});
		btnClose.addActionListener(new ActionListener() {
			/**
			 * Invoked when an action occurs.
			 *
			 * @param e
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnExport.addActionListener(new ActionListener() {
			/**
			 * Invoked when an action occurs.
			 *
			 * @param e
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				PDFExporter.writeChartToPDF(crtWorkerTasks, pnlWorkerTaskGraph.getWidth(), pnlWorkerTaskGraph.getHeight(), "workerTasksGraph.pdf");
				PDFExporter.writeChartToPDF(crtWorkerFlops, pnlWorkerFlopsGraph.getWidth(), pnlWorkerFlopsGraph.getHeight(), "workerFlopsGraph.pdf");
				PDFExporter.writeChartToPDF(crtQueues, pnlQueueGraph.getWidth(), pnlQueueGraph.getHeight(), "queueGraph.pdf");
			}
		});
	}


	/**
	 * Setup worker queue chart.
	 *
	 * @param root JPanel to place to chart.
	 */
	private void setupQueueChart(JPanel root) {
		crtQueues = ChartFactory.createXYLineChart(
				"Worker queue length",      // chart title
				"Time",                     // x axis label
				"Queue length / Worker",    // y axis label
				dataSetQueues,              // data
				PlotOrientation.VERTICAL,
				true,                       // include legend
				true,                       // tooltips
				false                       // urls
		);

		final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setBaseShapesVisible(true);
		crtQueues.getXYPlot().setRenderer(renderer);

		crtQueues.getXYPlot().setBackgroundPaint(Color.white);
		crtQueues.getXYPlot().setDomainGridlinePaint(Color.lightGray);
		crtQueues.getXYPlot().setRangeGridlinePaint(Color.lightGray);

		ChartPanel cp = new ChartPanel(crtQueues);
		cp.setPreferredSize(new Dimension(500, 500));
		cp.setMaximumDrawWidth(3000);
		cp.setMaximumDrawHeight(2000);
		root.add(cp);
	}


	/**
	 * Setup worker / task statistics chart.
	 *
	 * @param root JPanel to place to chart.
	 */
	private void setupWorkerTaskChart(JPanel root) {
		crtWorkerTasks = ChartFactory.createBarChart(
				"Worker / Tasks processed - Statistics",       // chart title
				"Worker",                  // domain axis label
				"Speed",                   // range axis label
				dataSetWorkerSpeed,        // data
				PlotOrientation.VERTICAL,  // orientation
				true,                      // include legend
				true,                      // tooltips?
				false                      // URLs?
		);

		CategoryPlot plot = crtWorkerTasks.getCategoryPlot();
		plot.setDataset(1, dataSetWorkerTasks);
		plot.mapDatasetToRangeAxis(1, 1);
		ValueAxis axis2 = new NumberAxis("# Tasks");
		plot.setRangeAxis(1, axis2);
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setBarPainter(new StandardBarPainter());
		renderer.setItemMargin(0.0);
		int i = 0;
		renderer.setSeriesPaint(i++, C_WORKER_SPEED);
		renderer.setSeriesPaint(i++, C_TASKS);
		plot.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_90);

		crtWorkerTasks.getCategoryPlot().setBackgroundPaint(Color.white);
		crtWorkerTasks.getCategoryPlot().setDomainGridlinePaint(Color.lightGray);
		crtWorkerTasks.getCategoryPlot().setRangeGridlinePaint(Color.lightGray);

		ChartPanel cp = new ChartPanel(crtWorkerTasks);
		cp.setPreferredSize(new Dimension(500, 500));
		cp.setMaximumDrawWidth(3000);
		cp.setMaximumDrawHeight(2000);
		root.add(cp);
	}


	/**
	 * Setup worker flops processed statistics chart.
	 *
	 * @param root JPanel to place to chart.
	 */
	private void setupWorkerFlopsChart(JPanel root) {
		crtWorkerFlops = ChartFactory.createBarChart(
				"Worker / FLOPS processed - Statistics",       // chart title
				"Worker",                  // domain axis label
				"Speed",                   // range axis label
				dataSetWorkerSpeed,        // data
				PlotOrientation.VERTICAL,  // orientation
				true,                      // include legend
				true,                      // tooltips?
				false                      // URLs?
		);

		CategoryPlot plot = crtWorkerFlops.getCategoryPlot();
		plot.setDataset(1, dataSetWorkerFlops);
		plot.mapDatasetToRangeAxis(1, 1);
		ValueAxis axis2 = new NumberAxis("# FLOPS Processed");
		plot.setRangeAxis(1, axis2);
		BarRenderer renderer = (BarRenderer) plot.getRenderer();
		renderer.setBarPainter(new StandardBarPainter());
		renderer.setItemMargin(0.0);
		int i = 0;
		renderer.setSeriesPaint(i++, C_WORKER_SPEED);
		renderer.setSeriesPaint(i++, C_TASKS);
		plot.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_90);

		crtWorkerFlops.getCategoryPlot().setBackgroundPaint(Color.white);
		crtWorkerFlops.getCategoryPlot().setDomainGridlinePaint(Color.lightGray);
		crtWorkerFlops.getCategoryPlot().setRangeGridlinePaint(Color.lightGray);

		ChartPanel cp = new ChartPanel(crtWorkerFlops);
		cp.setPreferredSize(new Dimension(500, 500));
		cp.setMaximumDrawWidth(3000);
		cp.setMaximumDrawHeight(2000);
		root.add(cp);
	}


	/**
	 * Read worker queue statistics file and build up the chart.
	 *
	 * @param workerQueueStatsFile Input file.
	 */
	public void showQueueStatistics(String workerQueueStatsFile) {
		// Clean filter panels
		pnlWorkers.removeAll();
		pnlWorkers.setLayout(new BoxLayout(pnlWorkers, BoxLayout.PAGE_AXIS));
		pnlQueues.removeAll();
		pnlQueues.setLayout(new BoxLayout(pnlQueues, BoxLayout.PAGE_AXIS));

		HashMap<String, HashMap<Integer, Integer>> currentQueueLength = new HashMap<>();

		// Read file line by line
		try (CSVReader reader = new CSVReader(new FileReader(workerQueueStatsFile))) {
			String[] line;
			reader.readNext(); // Skip first line
			while ((line = reader.readNext()) != null) {
				// "Worker","Timestamp","Queue","Length"

				String key = line[0];
				if (!seriesWorkerAllQueues.containsKey(key)) {
					addXYSeries(key, seriesWorkerAllQueues, pnlWorkers, true);
					currentQueueLength.put(key, new HashMap<>());
				}
				currentQueueLength.get(key).put(Integer.parseInt(line[2]), Integer.parseInt(line[3]));
				seriesWorkerAllQueues.get(key).add(
						Double.parseDouble(line[1]),
						currentQueueLength.get(key).values().stream().reduce((i, j) -> i + j).get());

				key = line[2] + "::" + line[0];
				if (!seriesWorkerSingleQueue.containsKey(key)) {
					addXYSeries(key, seriesWorkerSingleQueue, pnlQueues, false);
				}
				seriesWorkerSingleQueue.get(key).add(Double.parseDouble(line[1]), Integer.parseInt(line[3]));

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Repaint chart and filter parts
		crtQueues.fireChartChanged();
		pnlWorkers.validate();
		pnlQueues.validate();
		pnlFilter.updateUI();
	}


	/**
	 * Adds a XYSeries for worker queue statistic data. Additional a filter object will be placed.
	 *
	 * @param key    Key of new series / filter.
	 * @param map    Hashmap to add the new series.
	 * @param pnl    JPanel to place to filter.
	 * @param enable Is the series active or not.
	 */
	private void addXYSeries(final String key, final HashMap<String, XYSeries> map, JPanel pnl, boolean enable) {
		map.put(key, new XYSeries(key));
		if (enable) {
			dataSetQueues.addSeries(map.get(key));
		}
		Checkbox c = new Checkbox(key, enable);
		c.addItemListener(event -> {
			if (event.getStateChange() == ItemEvent.DESELECTED) {
				dataSetQueues.removeSeries(map.get(key));
			} else if (event.getStateChange() == ItemEvent.SELECTED) {
				dataSetQueues.addSeries(map.get(key));
			}
			crtQueues.fireChartChanged();
		});
		pnl.add(c);
	}


	/**
	 * Read worker statistics file and build up the chart.
	 *
	 * @param workerStatsFile Worker statistics file.
	 */
	private void showWorkerStatistics(String workerStatsFile) {
		try (CSVReader reader = new CSVReader(new FileReader(workerStatsFile))) {
			String[] line;
			reader.readNext(); // Skip first line
			while ((line = reader.readNext()) != null) {
				// "Worker","Speed","Cores","ProcessedTasks","ProcessedFlops"

				int i = 0;
				String worker = line[i++];
				double speed = Double.parseDouble(line[i++]);
				double cores = Double.parseDouble(line[i++]);
				int tasks = Integer.parseInt(line[i++]);
				double flops = Double.parseDouble(line[i++]);
				double overallSpeed = speed * cores;
				dataSetWorkerSpeed.addValue(overallSpeed, "Speed (cores * flops)", worker);
				dataSetWorkerTasks.addValue(null, "Dummy1", worker);
				dataSetWorkerTasks.addValue(tasks, "# Tasks processed", worker);
				dataSetWorkerSpeed.addValue(null, "Dummy2", worker);
				dataSetWorkerFlops.addValue(null, "Dummy3", worker);
				dataSetWorkerFlops.addValue(flops, "# FLOPS processed", worker);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		updateLegends();
		crtWorkerTasks.fireChartChanged();
		crtWorkerFlops.fireChartChanged();
	}


	/**
	 * Update legend for Worker Statistics
	 */
	private void updateLegends() {
		CategoryPlot plot = crtWorkerTasks.getCategoryPlot();
		LegendItemCollection legend = new LegendItemCollection();
		legend.add(plot.getRenderer().getLegendItem(0, 0));
		legend.add(plot.getRenderer().getLegendItem(1, 1));
		plot.setFixedLegendItems(legend);

		plot = crtWorkerFlops.getCategoryPlot();
		legend = new LegendItemCollection();
		legend.add(plot.getRenderer().getLegendItem(0, 0));
		legend.add(plot.getRenderer().getLegendItem(1, 1));
		plot.setFixedLegendItems(legend);
	}


	public static void main(String[] args) {
		WorkerStatisticsViewer viewer = new WorkerStatisticsViewer();

		String workerStatsFile = args.length > 1 ? args[0] : "worker_stats.csv";
		String workerQueueStatsFile = args.length > 1 ? args[1] : "worker_queue_stats.csv";

		//String workerStatsFile = "scenarios/02/results/5c_50041t_rv/worker_stats.csv";
		//String workerQueueStatsFile = "scenarios/02/results/5c_50041t_rv/worker_queue_stats.csv";

		viewer.showWorkerStatistics(workerStatsFile);
		viewer.showQueueStatistics(workerQueueStatsFile);
	}

	{
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
		$$$setupUI$$$();
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer
	 * >>> IMPORTANT!! <<<
	 * DO NOT edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		pnlRoot = new JPanel();
		pnlRoot.setLayout(new GridLayoutManager(2, 3, new Insets(0, 0, 0, 0), -1, -1));
		pnlWorkerGraphs = new JPanel();
		pnlWorkerGraphs.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
		pnlRoot.add(pnlWorkerGraphs, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		pnlWorkerTaskGraph = new JPanel();
		pnlWorkerTaskGraph.setLayout(new BorderLayout(0, 0));
		pnlWorkerGraphs.add(pnlWorkerTaskGraph, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		pnlWorkerFlopsGraph = new JPanel();
		pnlWorkerFlopsGraph.setLayout(new BorderLayout(0, 0));
		pnlWorkerGraphs.add(pnlWorkerFlopsGraph, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		pnlFilter = new JPanel();
		pnlFilter.setLayout(new GridLayoutManager(4, 1, new Insets(0, 0, 0, 0), -1, -1));
		pnlRoot.add(pnlFilter, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_NORTHWEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		lblQueues = new JLabel();
		lblQueues.setText("Queues:");
		pnlFilter.add(lblQueues, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		final JScrollPane scrollPane1 = new JScrollPane();
		pnlFilter.add(scrollPane1, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, new Dimension(-1, 100), null, null, 0, false));
		pnlQueues = new JPanel();
		pnlQueues.setLayout(new BorderLayout(0, 0));
		scrollPane1.setViewportView(pnlQueues);
		final JScrollPane scrollPane2 = new JScrollPane();
		pnlFilter.add(scrollPane2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, new Dimension(-1, 100), null, null, 0, false));
		pnlWorkers = new JPanel();
		pnlWorkers.setLayout(new BorderLayout(0, 0));
		scrollPane2.setViewportView(pnlWorkers);
		workersCheckBox = new JCheckBox();
		workersCheckBox.setSelected(true);
		workersCheckBox.setText("Workers:");
		pnlFilter.add(workersCheckBox, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		pnlQueueGraph = new JPanel();
		pnlQueueGraph.setLayout(new BorderLayout(0, 0));
		pnlRoot.add(pnlQueueGraph, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
		pnlButtons = new JPanel();
		pnlButtons.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 0));
		pnlRoot.add(pnlButtons, new GridConstraints(1, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
		btnExport = new JButton();
		btnExport.setText("Export charts to PDF");
		pnlButtons.add(btnExport);
		btnClose = new JButton();
		btnClose.setText("Close");
		pnlButtons.add(btnClose);
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return pnlRoot;
	}
}
