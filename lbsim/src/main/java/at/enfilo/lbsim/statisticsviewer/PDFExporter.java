package at.enfilo.lbsim.statisticsviewer;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.awt.PdfGraphics2D;
import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import org.jfree.chart.JFreeChart;

import java.awt.geom.Rectangle2D;
import java.io.FileOutputStream;


/**
 * Export JFreeChart to a PDF Document.
 */
public class PDFExporter {

	/**
	 * Export / write a JFreeChart to a PDF document.
	 * @param chart	chart to export
	 * @param width	width of chart.
	 * @param height	height of chart.
	 * @param fileName	PDF File name.
	 */
	public static void writeChartToPDF(JFreeChart chart, int width, int height, String fileName) {
		PdfWriter writer = null;

		Document document = new Document();
		document.setPageSize(new Rectangle(0, 0, width, height));

		try {
			writer = PdfWriter.getInstance(document, new FileOutputStream(fileName));
			document.open();
			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(height, width);
			PdfGraphics2D graphics2D = new PdfGraphics2D(template, width, height, new DefaultFontMapper());
			template.setWidth(width);
			template.setHeight(height);
			Rectangle2D rectangle2d = new Rectangle2D.Double(0, 0, width, height);
			chart.draw(graphics2D, rectangle2d);
			graphics2D.dispose();
			contentByte.addTemplate(template, 0, 0);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			document.close();
		}
	}
}
